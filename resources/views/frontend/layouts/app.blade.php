<!DOCTYPE html>
@langrtl
    <html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        @stack('after-styles')
        

    </head>
    <body>
        <div id="app">
            <div class="nav nav-bar" style="background-color:rgb(27, 27, 27); padding: 20px"><h5 style="color: white"><b>Gestão Parts</b> - Login</h5></div>
            @include('includes.partials.logged-in-as')
           <!-- @ include('frontend.includes.nav') -->

            <div class="container-fluid" >
                @include('includes.partials.messages')

                @yield('content')
            </div><!-- container -->
        </div><!-- #app -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/frontend.js')) !!}
        @stack('after-scripts')

        @include('includes.partials.ga')

        <div class="footer-fixed footer">
            <div class="col-1 mar-top-15">

                <img style="max-height: 50px; float: left; margin: 5px"
                     src="{{asset('/img/backend/marktplace/gestao-parts-logo.png')}}"/>
                <span> </span>
            </div>
            <div class="col-11 mar-top-15">
                    © {{ date('Y')}} Copyright Gestão Parts
            </div>
        </div>
    @yield('estilo-login')
    </body>
</html>
