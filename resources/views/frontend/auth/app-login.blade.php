@extends('frontend.layouts.app')

@section('title', app_name() . ' | Login')

@section('content')



    <div style="margin-top: 1%;">

        <div class="row">
            <div class="col-12 text-center">
                <h2 style="color: #6c757d;"><i style="color: orange" class="fa fa-lock"></i> Acesse sua conta </h2>
                <hr><br>
            </div>
        </div>

        <div class="row ">
            <div class="col-xs-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                                <h1>Entrar</h1>
                                <p class="text-muted">Informe seus dados para entrar</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    <input type="email" name="email" class="form-control" placeholder="Usuário">
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <input type="password" name="password" class="form-control" placeholder="Senha">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-primary px-4">Entrar</button>

                                    </div>
                                    <div class="col-6 text-right">
                                        <button type="button" class="btn btn-link px-0">Qual a minha senha?</button>
                                    </div>
                                </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection
@section('estilo-login')

@endsection
