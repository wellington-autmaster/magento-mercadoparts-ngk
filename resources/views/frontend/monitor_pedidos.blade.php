<!DOCTYPE html>
@langrtl
<html lang="pt-BR" dir="rtl">
@else
    <html lang="pt-BR">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
        <title>@yield('title', app_name())</title>
        <meta name="description"
              content="@yield('meta_description', 'Gestão Parts - Monitor de separação de peças para gestores.')">
        <meta name="author" content="@yield('meta_author', 'Gestão Parts')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}
        {{--<script src="/node_modules/chart.js"></script>--}}
        @stack('after-styles')


    </head>
    <body class="fonte-monitor" style="background-color: #1a2226; ">
    <div id="app">
    {{--<vc-container v-bind:empresa="{{$empresa }}" v-bind:totais="{{ $totais }}"></vc-container>--}}
        <vc-spss-modal empresa="{{ $empresa }}"></vc-spss-modal>
        <div class="container-fluid"  >

         <vc-spss-header v-bind:totais="{{ $totais }}" empresa="{{ $empresa }}" ></vc-spss-header>

         <vc-spss-pedidos empresa="{{ $empresa }}" ></vc-spss-pedidos>
        </div>
    </div>

    <div style="background-color: black; opacity: 0.9" class="footer" >
        <img style="max-height: 50px; float: left; margin: 5px"
             src="{{asset('/img/backend/marktplace/gestao-parts-logo.png')}}"/>
        <h4 style="float: right;margin: 10px">Especialista em gestão empresarial.</h4>
    </div>


    @stack('before-scripts')
    {!! script(mix('js/frontend.js')) !!}
    @stack('after-scripts')


    <script>


        //  var pedidos = new Chart($('#card-chart1'), {
      /*  var pedidos = new Chart($('#chart-pedidos'), {
            type: 'bar', //horizontalBar
            data: {
                labels: ['Janeiro', 'Fevereiro', 'março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                datasets: [{
                    label: 'Pedidos',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: [78, 81, 80, 45, 34, 12, 40]
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {display: false},
                scales: {xAxes: [{display: false, barPercentage: 0.6}], yAxes: [{display: false}]}
            }
        });

        var separacao = new Chart($('#chart-separacao'), {
            type: 'horizontalBar', //horizontalBar
            data: {
                labels: ['Dentro da meta', 'Atrasados'],
                datasets: [{
                    label: 'Pedidos',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: ['21', '70']
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {display: false},
                scales: {xAxes: [{display: false, barPercentage: 0.6}], yAxes: [{display: false}]}
            }
        });

        Chart.defaults.global.pointHitDetectionRadius = 1;
        Chart.defaults.global.tooltips.enabled = true;
        Chart.defaults.global.tooltips.mode = 'nearest';
        Chart.defaults.global.tooltips.position = 'average';
        ///////////

        var conferencia = new Chart($('#chart-conferencia'), {
            type: 'horizontalBar', //horizontalBar
            data: {
                labels: ['Dentro da meta', 'Atrasados'],
                datasets: [{
                    label: 'Pedidos',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: ['9', '80']
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {display: false},
                scales: {xAxes: [{display: false, barPercentage: 0.6}], yAxes: [{display: false}]}
            }
        });

        ///////////////

        //  var pedidos = new Chart($('#card-chart1'), {
        var expedicao = new Chart($('#chart-expedicao'), {
            type: 'horizontalBar', //horizontalBar
            data: {
                labels: ['Dentro da meta', 'Atrasados'],
                datasets: [{
                    label: 'Pedidos',
                    backgroundColor: 'rgba(255,255,255,.2)',
                    borderColor: 'rgba(255,255,255,.55)',
                    data: ['7', '81']
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {display: false},
                scales: {xAxes: [{display: false, barPercentage: 0.6}], yAxes: [{display: false}]}
            }
        });

        Chart.defaults.global.pointHitDetectionRadius = 1;
        Chart.defaults.global.tooltips.enabled = true;
        Chart.defaults.global.tooltips.mode = 'nearest';
        Chart.defaults.global.tooltips.position = 'average';
*/

    </script>

    <style>


        .fonte-monitor {
            font-family: 'Open Sans', sans-serif;
        }






    </style>
    </body>
    </html>
<script>

</script>

