<footer class="app-footer footer-fixed">
    <strong>Copyright  &copy; {{ date('Y') }}
        <a href="https://www.gestaoparts.com.br">Gestão Parts</a>
    </strong> Todos os direitos reservados.

    <span class="float-right">Criado por <a href="https://www.gestaoparts.com.br">Gestão Parts</a></span>


</footer>