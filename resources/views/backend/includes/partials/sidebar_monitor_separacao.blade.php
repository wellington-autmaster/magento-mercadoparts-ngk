<li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/monitor/index'), 'open') }}">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="icon-screen-desktop"></i> Paineis de monitoramento
    </a>

    <ul class="nav-dropdown-items">

        <li class="nav-item">
            <a class="nav-link " href="{{ route('frontend.monitor', Auth()->user()->empresa_codigo) }}" target="_blank">
                <i class="icon-screen-desktop"></i> Senhas
            </a>
        </li>
        @include('backend.includes.partials.sidebar_monitor_servico')
        @include('backend.includes.partials.sidebar_monitor_pedidos')
        <li class="nav-item">
            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/monitor/index')) }}" href="{{ route('admin.monitor.index') }} " >
                <i class="fa fa-cog"></i> Configurações
            </a>
        </li>



    </ul>
</li>
