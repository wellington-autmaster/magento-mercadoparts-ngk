<li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/marktplace'), 'open') }}">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="icon-organization"></i> Marktplaces
    </a>

    <ul class="nav-dropdown-items">
        <li class="nav-item">
            <a class="nav-link {{ active_class(Active::checkUriPattern('/admin/produtos')) }}" href="{{ route('admin.SSprodutosPesquisa') }}">
                Produtos
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/marktplace')) }}" href="{{ route('admin.marktplace') }}">
                Marktplaces
            </a>
        </li>

    </ul>
</li>