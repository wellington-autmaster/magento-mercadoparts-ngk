<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

        <span class="d-md-down-none">{{ $logged_in_user->empresa->codigo .' '. $logged_in_user->empresa->fantasia }}</span>
    </a>

    <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
            <strong>Empresas </strong>
        </div>

        @foreach(empresas() as $empresa)
        <a class="dropdown-item" href="{{Route('admin.getEmpresas',$empresa->id)}}"><i class="fa fa-exchange"></i>{{ $empresa->codigo .' - '. $empresa->fantasia }}</a>
        @endforeach

    </div>
</li>