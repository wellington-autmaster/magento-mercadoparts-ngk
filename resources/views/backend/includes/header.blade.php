<header class="app-header navbar" >
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" data-toggle="sidebar-show" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" data-toggle="sidebar-show" type="button">☰</button>

    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.index') }}"><i class="icon-home"></i></a>
        </li>

        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">{{ __('navs.frontend.dashboard') }}</a>
        </li>

        @if (config('locale.status') && count(config('locale.languages')) > 1)
            <li class="nav-item px-3 dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="d-md-down-none">{{ __('menus.language-picker.language') }} ({{ strtoupper(app()->getLocale()) }})</span>
                </a>

                @include('includes.partials.lang')


            </li>
        @endif

        @include('backend.includes.partials.mudar_empresa')

    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">

            <vc-notificacao></vc-notificacao>

        </li>

        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-list"></i></a>
        </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ $logged_in_user->picture }}" class="img-avatar" alt="{{ $logged_in_user->email }}">
                <span class="d-md-down-none">{{ $logged_in_user->full_name }}</span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">

                <div class="dropdown-header text-center">
                    <strong>Minha Conta</strong>
                </div>
                <a class="dropdown-item" href="{{ url('/admin/auth/user/'.$logged_in_user->id) }}"><i class="fa fa-search"></i> Visualizar</a>
                <a class="dropdown-item" href="{{ url('/admin/auth/user/'.$logged_in_user->id.'/edit') }}"><i class="fa fa-pencil"></i> Editar</a>
                <a class="dropdown-item" href="{{ url('/admin/auth/user/'.$logged_in_user->id.'/password/change') }}"><i class="fa fa-lock"></i> Alterar senha</a>
                <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}"><i class="fa fa-sign-out"></i> Sair</a>
            </div>
        </li>
    </ul>

    <button class="navbar-toggler aside-menu-toggler" type="button">☰</button>
</header>