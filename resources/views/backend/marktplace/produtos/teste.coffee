#tabela de produtos
module.exports = (robot) ->
  robot.hear /tabela de (produto(|s)|ite(n|m)(|s))/i, (res) ->
    res.send "Tabela de Produtos é *PCCDITE0* . "


  #Agendamento de cliente
  robot.hear /tabela de agendamento(|s) de clinte(|s)/i, (res) ->
    res.send "A tabela é *PCAGECLI* . "


  #Agendamento de veículos
  robot.hear /tabela de agendamento(|s) de veiculo(|s)/i, (res) ->
    res.send "A tabela é  *PCAGEVEI* . "


  #Aplicação dos produtos
  robot.hear /tabela de aplica(c|ç)(a|ã)o/i, (res) ->
    res.send "A tabela é  *PCCDAPL0* . "


  #Cadastro de Bancos
  robot.hear /tabela de cadastro de banco(|s)/i, (res) ->
    res.send "A tabela é *PCCDBAN0* . "


  #Complemento cadastro de cliente obs e clientes
  robot.hear /tabela de cadastro de cliente(|s)/i, (res) ->
    res.send "A tabela de cadastro de clinetes é *PCCDCLI0*, do complemento é *PCCDCLC0* e das observações *PCCDCLO0* . "


  #Cadastro de entidades
  robot.hear /tabela (|de cadastro) de entidade(|s)/i, (res) ->
    res.send "A tabela de cadastro de entidaes é *PCCDENT0* . "

  #Erros do sistema PCCDERRO
  robot.hear /tabela  de erro(|s)/i, (res) ->
    res.send "A tabela de erros e *PCCDERRO* . "


  #Relacionamento de produtos com fornecedores
  robot.hear /tabela  de relacionamento de produto(|s) com fornecedor(|es)/i, (res) ->
    res.send "A tabela de relacionamento de produtos com fornecedores e *PCCDFIT0* . "


  #Cadastro de históricos de caixa
  robot.hear /tabela  de historico de caixa/i, (res) ->
    res.send "A tabela de historico de caixa e *PCCDHIS0* . "

  #Cadastro de histórico de precos
  robot.hear /tabela  de historico de pre(|ç)o(|s)/i, (res) ->
    res.send "A tabela de historico de preços e *PCCDITH0* . "

  #Localização dos itens e estoque mínimo e máximo
  robot.hear /tabela  de localizacao/i, (res) ->
    res.send "A tabela de localização de produtos, estoque minimo e máximo é *PCCDLOC0* . "

  #Dados fiscais produto (tributação, alíquota, etc.).
  robot.hear /tabela de (tributacao|imposto|aliquota|icms)/i, (res) ->
    res.send "A tabela de dados fiscais dos produtos é *PCCDPITE* . "

  #Dados referente Icms nas notas fiscais, base de cálculo, valor do icms, alíquota e planilha.
  robot.hear /tabela de (tributacao|imposto|aliquota|icms|notas)/i, (res) ->
    res.send "Dados referente Icms nas notas fiscais, base de cálculo, valor do icms, alíquota e planilha *PCCDPITE* . "

  #Conferencia de entrada e saída
  robot.hear /tabela de conferencia/i, (res) ->
      res.send "Conferencia de entrada e saída  *PCCMCES0* . "

  #Senha masterBox
  robot.hear /senha masterbox/i, (res) ->
    res.send "*Anydesk:* g1g@bytemasterbox \n *Linux:* master \n *Sudo:* master "




