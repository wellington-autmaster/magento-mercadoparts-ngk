@extends('backend.layouts.app')

@section ('title', 'Gerenciamento de Marktplaces')

@section('breadcrumb-links')
    @include('backend.marktplace.includes.breadcrumb-links')
@endsection


@section('content')

<div class="card">
        <h4 class="card-header"> Gerenciamento de Marktplaces <small class="text-muted"> Configurações gerais</small></h4>
        <div class="card-body">
            <div class="row">
                @foreach ($marktplaces as $marktplace)
                    <div class="col-sm-4">
                        <div class="card">
                                <img class="card-img-top img-thumbnail img-fluid" src="{{ url($marktplace->imagem) }}" alt="Card image cap">
                            <div class="card-body">
                              <h4 class="card-title">{{  $marktplace->nome  }}</h4>
                              <a href="{{parse_url($marktplace->url, PHP_URL_SCHEME).'://'. parse_url($marktplace->url,PHP_URL_HOST) }}">{{ parse_url($marktplace->url,PHP_URL_HOST) }}</a>
                              <p class="card-text">{{  $marktplace->descricao  }}</p>
                              
                              <p>Ultima atualização {{ $marktplace->updated_at->diffForHumans() }}</p>
                              <a href="#" class="card-link">
                                    @if($marktplace->status) 
                                        <a href="{{ route('admin.marktplace.editar',$marktplace->id) }}" class="btn btn-primary  btn-block"><i
                                        class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Ativo"></i> Configurações</a>

                                    @else 
                                        <a href="{{ route('admin.marktplace.editar',$marktplace->id) }}" class="btn btn-success  btn-block"><i
                                        class="fa fa-plus-circle" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Instalar "></i> Instalar</a>
                                    @endif
                              </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
      </div>
      
    
@endsection