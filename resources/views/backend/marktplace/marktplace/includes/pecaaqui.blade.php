<div class="row mt-4 mb-4">
        <div class="col">

            <div class="form-group row">
                {{ html()->label('Empresa')->class('col-md-2 form-control-lavel')->for('empresa')}}

                <div class="col-md-10">
                    {{ html()->text('empresa')->class('form-control')->attribute('disabled')->value( $marktplace->empresa)}}
                </div>
            </div>

            <div class="form-group row">


                {{ html()->label('Usuário')->class('col-md-2 form-control-label')->for('usuario') }}

                <div class="col-md-10">
                    {{ html()->text('usuario')
                        ->class('form-control')
                        ->placeholder('Usuário')
                        ->attribute('maxlength', 191)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group row">
                {{ html()->label('Senha')->class('col-md-2 form-control-label')->for('senha') }}

                <div class="col-md-10">
                    {{ html()->text('senha')
                        ->class('form-control')
                        ->placeholder('Senha')
                        ->attribute('maxlength', 191)
                        ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            


            <div class="checkbox">
                Habilitar
                {{ html()->label(
                        html()->checkbox('status')
                              ->class('switch-input')
                                 ->class('switch switch-sm switch-3d switch-primary')
                   )}}

            </div>



        </div><!--col-->
    </div><!--row-->