<hr>
<h4>Autenticação</h4>
<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            {{ html()->label('Empresa')->class('col-md-2 form-control-lavel')->for('empresa')}}
            <div class="col-md-10">
                {{ html()->text('empresa')->class('form-control')->attribute('disabled')->value( $marktplace->empresa)}}
            </div>
        </div>

        <div class="form-group row">
            {{ html()->label('Usuário')->class('col-md-2 form-control-label')->for('usuario') }}
            <div class="col-md-10">
                {{ html()->text('usuario')
                    ->class('form-control')
                    ->placeholder('Usuário')
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Senha')->class('col-md-2 form-control-label')->for('senha') }}
            <div class="col-md-10">
                {{ html()->text('senha')
                    ->class('form-control')
                    ->placeholder('Senha')
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Cient ID')->class('col-md-2 form-control-label')->for('client_id') }}
            <div class="col-md-10">
                {{ html()->text('client_id')
                    ->class('form-control')
                    ->placeholder('Client ID')
                    ->attribute('maxlength', 200)  }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Cient Secret')->class('col-md-2 form-control-label')->for('client_secret') }}
            <div class="col-md-10">
                {{ html()->text('client_secret')
                     ->class('form-control')
                     ->placeholder('Client Secret')
                     ->attribute('maxlength', 200)
                     ->required()

                     }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('URL')->class('col-md-2 form-control-label')->for('url') }}
            <div class="col-md-10">
                {{ html()->text('url')
                     ->class('form-control')
                     ->placeholder('http://dominio.com.br')
                     ->attribute('maxlength', 200)
                     ->required() }}
            </div><!--col-->
        </div> <!--form-group-->

        <hr>
        <h4>Pedidos e Clientes</h4>


        <div class="form-group row">
            {{ html()->label('Status do pedido a importar')->class('col-md-2 form-control-label')->for('status_pedido_importar') }}
            <div class="col-md-10">
                {{ html()->select('status_pedido_importar', $marktplace->status_pedidos_ecom, $marktplace->status_pedido_importar)
                     ->class('form-control')
                     ->attribute('maxlength', 200)
                }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Status do pedido após importar')->class('col-md-2 form-control-label')->for('status_pos_importa_pedido') }}
            <div class="col-md-10">
                {{ html()->select('status_pos_importa_pedido', $marktplace->status_pedidos_ecom, $marktplace->status_pos_importa_pedido)
                     ->class('form-control')
                     ->attribute('maxlength', 200)
                     }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Status do pedido após gerar NF-e')->class('col-md-2 form-control-label')->for('status_pos_nfe_pedido') }}
            <div class="col-md-10">
                {{ html()->select('status_pos_nfe_pedido', $marktplace->status_pedidos_ecom)
                     ->class('form-control')
                     ->attribute('maxlength', 200)
                     }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Limite de crédio do cliente R$')->class('col-md-2 form-control-label')->for('limite_credito_cliente') }}
            <div class="col-md-10">
                {{ html()->text('limite_credito_cliente')
                    ->class('form-control')
                    ->placeholder('R$300,00')
                    ->attribute('maxlength', 8)
                    ->attribute('number')
                 }}
            </div><!--col-->

        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Forma de pagamento SSPlus')->class('col-md-2 form-control-label')->for('forma_pagamento_ss') }}
            <div class="col-md-10">
                {{ html()->text('forma_pagamento_ss')
                    ->class('form-control')
                    ->placeholder('43')
                    ->attribute('maxlength', 191)
                 }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label('Classe de produtos para não enviar ao e-commerce')->class('col-md-2 form-control-label')->for('classe_nao_enviar') }}
            <div class="col-md-10">
                {{ html()->text('classe_nao_enviar')
                    ->class('form-control')
                    ->placeholder("A,B,C")
                    ->attribute('maxlength', 191)

                 }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="checkbox">
            {{ html()->label('Atualiza cadastro de cliente no SSPlus')->class('col-md-3 form-control-label ')->for('atualiza_cliente_ss') }}
            {{ html()->label(
                html()->checkbox('atualiza_cliente_ss')
                    ->class('switch-input')
                    ->class('switch switch-sm switch-3d switch-primary')->attributes(["data-toggle" =>"tooltip","data-placement"=>"right",
                    "title"=>"Ative para atualizar o cadastro de cliente no SSPlus com base no cadastro do e-commerce"]) )}}
        </div>
        <hr>
<h4>Produtos</h4>


        {{-- Config. Envio de promoções do SSPlus ao e-commerce--}}
        <div class="checkbox">
            {{ html()->label('Promoções')->class('col-md-3 form-control-label ')->for('enviar_promocao') }}
            {{ html()->label(
                html()->checkbox('enviar_promocao')
                    ->class('switch-input')
                    ->class('switch switch-sm switch-3d switch-primary')->attributes(["data-toggle" =>"tooltip","data-placement"=>"right",
                    "title"=>"Ative para enviar as promoções do SSPlus ao e-Commerce"]) )}}
        </div>

       {{-- // Config. Adicionar a Marca do produto junto com o nome. --}}
        <div class="checkbox">
            {{ html()->label('Mostrar marca na descrição')->class('col-md-3 form-control-label ')->for('marca_na_descricao') }}
            {{ html()->label(
                html()->checkbox('marca_na_descricao')
                    ->class('switch-input')
                    ->class('switch switch-sm switch-3d switch-primary')->attributes(["data-toggle" =>"tooltip","data-placement"=>"right",
                    "title"=>"Ative para que a Marca seja exibida logo após o nome do produto no e-Commerce"]) )}}
        </div>

        {{--  Config. Atualizar descrição (Aplicacao) do produto no e-Commerce --}}
        <div class="checkbox">
            {{ html()->label('Atualizar descrição do produto')->class('col-md-3 form-control-label ')->for('atualizar_descricao') }}
            {{ html()->label(
                html()->checkbox('atualizar_descricao')
                    ->class('switch-input')
                    ->class('switch switch-sm switch-3d switch-primary')->attributes(["data-toggle" =>"tooltip","data-placement"=>"right",
                    "title"=>"Ative para que a descrição do produto (Aplicação) seja atualizada no e-Commerce"]) )}}
        </div>

        {{-- Config. Inativar produtos com estoque zerado no e-Commerce --}}
        <div class="checkbox">
            {{ html()->label('Inativar produtos sem estoque')->class('col-md-3 form-control-label ')->for('inativa_produto_sem_estoque') }}
            {{ html()->label(
                html()->checkbox('inativa_produto_sem_estoque')
                    ->class('switch-input switch-pill' )
                    ->class('switch switch-sm switch-3d switch-primary')->attributes(["data-toggle" =>"tooltip","data-placement"=>"right",
                    "title"=>"Ative para não exibir produtos sem estoque no e-Commerce"]) )}}
        </div>

        {{-- Config. Inativar produtos com estoque zerado no e-Commerce --}}

        <hr>
        <div class="checkbox">
            {{ html()->label('Habilitar')->class('col-md-2 form-control-label')->for('Habilitar') }}
            {{ html()->label(
                html()->checkbox('status')
                    ->class('switch-input')
                    ->class('switch switch-sm switch-3d switch-primary') )}}
        </div>

        <div>
            <contrato-modulo modulo="Opencart" cnpj="{{Auth()->user()->empresa->cgc}}"  />
        </div>


        </div><!--col-->

    </div><!--row-->
