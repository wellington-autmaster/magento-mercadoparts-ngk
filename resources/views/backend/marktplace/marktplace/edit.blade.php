@extends('backend.layouts.app')

@section ('title', 'Gerenciamento de Marktplaces')

@section('breadcrumb-links')
    @include('backend.marktplace.includes.breadcrumb-links')
@endsection


@section('content')
    {{ html()->modelForm($marktplace, 'PATCH', route('admin.marktplace.update', $marktplace))->class('form-horizontal')->open() }}
    <input hidden name="id" value="{{$marktplace->id}}">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                       Configuração de Marktplace
                    <small class="text-muted"> {{ $marktplace->nome }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            
                @if($marktplace->codigo == 'plugto')
                    @include('backend.marktplace.marktplace.includes.plugto')
                @elseif($marktplace->codigo == 'pecaqui')
                    @include('backend.marktplace.marktplace.includes.pecaaqui')
                @elseif($marktplace->codigo == 'opencart')
                    @include('backend.marktplace.marktplace.includes.opencart')
                @elseif($marktplace->codigo == 'magento23')
                     @include('backend.marktplace.marktplace.includes.magento.magento23')
                @endif
            

            
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.marktplace'), 'Cancelar') }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit('Atualizar') }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection

