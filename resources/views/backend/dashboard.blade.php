@extends('backend.layouts.app')

@section('content')


    <div class="row">
        <div class="col-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">e-Commerce</h5>
                    <p class="card-text">Quando clicar em processar, o sistema fará uma busca no SSPlus por produtos que tiveram movimentações na data de hoje,
                    caso for necessário enviar todos os produtos, ative a atualização geral de produtos, lembrando que isso poderá afetar o desempenho do servidor dependendo da
                        quantidade de produtos<br> <b>Antes de atualizar manualmente, verifique se não esta próximo do horário de atualização automática.</b> </p>
                    <p><b>Horários de atualização:</b><br> <b>Produtos:</b> 06:00 | 10:00 | 12:00 | 16:00 | 20:00 - <b>Pedidos:</b> 11:00 | 14:00 | 17:00 | 02:00 - <b>Plugg-To:</b>  07:00 | 11:00 |
                        15:00 | 18:00 </p>




                    <a href="{{ route('admin.processarFila') }}" class="btn btn-primary  "><i class="fa fa-refresh"></i> Produtos</a>
                    <a href="{{ route('admin.processarFilaPedidos') }}" class="btn btn-primary  "><i class="fa fa-refresh"></i> Pedidos</a>
                    <a href="{{ url('/horizon') }}" class="btn btn-primary  "><i class="fa fa-dashboard"></i> Processos</a>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title hidden"></h5>

                    <vc-marktplace-atualiza-geral></vc-marktplace-atualiza-geral>
                    <p class="card-text"></p>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>{{ __('strings.backend.dashboard.welcome') }} {{ $logged_in_user->name }}!</strong>
                </div><!--card-header-->
                <div class="card-block">
                   <p> Bem vindo a sua plataforma de integração do SSPlus com Marktplaces.</p>
                    <p>Alavanque suas vendas nos maiores marktplaces disponiveis no Brasil</p>
                    <hr>
                    <img src="/img/backend/marktplace/infografico-marktplace.png" title="Marktplace Autmaster">
                    <p>Para maiores informações entre em contato com o comercial em <a href="www.autmaster.com.br">Autmaster.com.br</a><br>
                </div><!--card-block-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

@endsection