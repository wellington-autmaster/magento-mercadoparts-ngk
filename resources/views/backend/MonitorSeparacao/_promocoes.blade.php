<div class="alert " role="alert">
    <h4>Promoções</h4> Configuração das promoções exibidas no painel.
    <hr>
</div>
<h5>Descrição do produto em promoção</h5>

<div class="form-group">
    <label for="proDescSize">Tamanho da fonte Descrição do Produto</label>
    <div class="input-group">
        <input type="number" name="proDescSize" value="{{ $monitor->proDescSize }}" class="form-control"
               id="proDescSize" aria-describedby="proDescSizeHelp">
        <div class="input-group-append ">
            <span class="input-group-text" id="proDescSizeHelp">PX</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="proDescColor">Cor da Descrição do Produto</label>
    <input type="color" name="proDescColor" value="{{ $monitor->proDescColor }}" class="form-control" id="proDescColor"
           aria-describedby="proDescColorHelp">
    <small id="proDescColorHelp" class="form-text text-muted icon-info"> Tamanho do texto que exibe a descrição do
        produto em promoção.
    </small>
</div>
<br>
<h5>Descrição do preço do produto em promoção</h5>
<hr>
<div class="form-group">
    <label for="proDescSize">Tamanho da fonte do preço do Produto</label>
    <div class="input-group">
        <input type="number" name="proValSize" value="{{ $monitor->proValSize }}" class="form-control"
               id="proValSize" aria-describedby="proValSizeHelp">
        <div class="input-group-append ">
            <span class="input-group-text" id="proValSizeHelp">PX</span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="proValColor">Cor do preço do Produto</label>
    <input type="color" name="proValColor" value="{{ $monitor->proValColor }}" class="form-control" id="proValColor"
           aria-describedby="proValColorHelp">
    <small id="proValColorHelp" class="form-text text-muted icon-info"> Cor do preço do produto em promoção
    </small>
</div>
<hr>

<div class="form-group">
    <label for="img">Imagem de fundo do monitor</label>
    <input type="file" name="img"  class="form-control" id="img"
           aria-describedby="imgHelp">
    <img id="myImg" src="{{asset($monitor->imgBackground)}}"  width="100" alt=""/>
    <small id="imgHelp" class="form-text text-muted icon-info"> Imagem que será mostrada como plano de fundo do monitor de separação
    </small>
</div>


<hr>
<div class="form-group">
    <label for="imgLogo">Logotipo da empresa</label>
    <input type="file" name="imgLogo"  class="form-control" id="imgLogo"
           aria-describedby="imgLogoHelp">
    <img id="imgLogo" src="{{asset($monitor->imgLogo)}}"  width="100" alt=""/>
    <small id="imgLogoHelp" class="form-text text-muted icon-info"> Logotipo da empresa para mostrar no monitor
    </small>
</div>

