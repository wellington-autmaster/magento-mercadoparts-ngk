<div class="alert " role="alert">
    <h4>Chamada de Senhas</h4>
    Configuração das senhas para retirada de mercadorias do balcão.
    <hr>
</div>

<div class="form-group">
    <label for="qtdSenhas">Número de senhas principais para exibir:</label>
    <input type="number" class="form-control" name="qtdsenhas" id="qtdSenhas" aria-describedby="qtdSenhasHelp" value="{{ $monitor->qtdSenhas }}" placeholder="Insira a quantidade de senhas">
    <small id="qtdSenhasHelp" class="form-text text-muted icon-info"> Informa a quantidade de senhas em processamento e concluidas que deve aparecer no monitor, de acordo com a resolução.</small>
</div>

<div class="form-group">
    <label for="qtdsenhasaguardando">Número de senhas aguardando processamento para exibir:</label>
    <input type="number" class="form-control" name="qtdsenhasaguardando" id="qtdsenhasaguardando" aria-describedby="qtdSenhasaguardandoHelp" value="{{ $monitor->qtdsenhasaguardando }}" placeholder="Insira a quantidade de senhas">
    <small id="qtdSenhasaguardandoHelp" class="form-text text-muted icon-info"> Informa a quantidade de senhas aguardando processamento que deve aparecer no monitor, de acordo com a resolução.</small>
</div>


<div class="form-group ">
    <label for="modoentregabalcao">Tipos de separações:</label>

    <select class="form-control" name="modoentregabalcao" id="modoentregabalcao" aria-describedby="modoentregabalcaoHelp">
        <option @if($monitor->modoentregabalcao == 1)selected @endif value="1">Balcão</option>
        <option @if($monitor->modoentregabalcao == 0)selected @endif value="0">Motoboy e Balcão</option>
    </select>
    <small id="modoentregabalcaoHelp" class="form-text text-muted icon-info"> Informe se o monitor deve mostrar todas as separações ou apenas para retirada no balcão..</small>
</div>

<div class="form-group ">
    <label for="conferencia">Controlar senha até: </label>

    <select class="form-control" name="conferencia" id="conferencia" aria-describedby="conferenciaHelp">
        <option @if($monitor->conferencia == true)selected @endif value="1">Conferência de Mercadorias</option>
        <option @if($monitor->conferencia == false)selected @endif value="0">Separação de Mercadorias</option>
    </select>
    <small id="conferenciaHelp" class="form-text text-muted icon-info"> Informe até que ponto deseja receber senhas no monitor.</small>
</div>


<div class="form-group ">
    <label for="senhas">Senhas:</label>

        <select class="form-control" name="senhas" id="senhas" aria-describedby="senhasHelp">
            <option @if($monitor->senhas == 1)selected @endif value="1">Ativo</option>
            <option @if($monitor->senhas == 0)selected @endif value="0">Desligado</option>
        </select>
    <small id="SenhasHelp" class="form-text text-muted icon-info"> Ative caso deseje mostrar as senhas para retirada de mercadorias no balcão.</small>
</div>

