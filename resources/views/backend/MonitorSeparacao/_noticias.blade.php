<div class="alert " role="alert">
    <h4>Notícias</h4> Configuração das notícias a exibir no painel.
    <hr>
</div>



<div class="form-group ">
    <label for="noticias_fonte">Fonte de notícias:</label>

    <select class="form-control" name="noticias_fonte" id="noticias_fonte" aria-describedby="noticias_fonteHelp">
        <option @if ( $monitor->noticias_fonte == 'globo') selected @endif value="globo">Globo</option>
        <option @if ( $monitor->noticias_fonte == 'google-news-br') selected @endif value="google-news-br">Google News (Brasil)</option>
        <option @if ( $monitor->noticias_fonte == 'blasting-news-br') selected @endif value="blasting-news-br">Blasting News (BR)</option>
        <option @if ( $monitor->noticias_fonte == 'info-money') selected @endif value="info-money">InfoMoney</option>
    </select>
    <small id="noticias_fonteHelp" class="form-text text-muted icon-info"> Escolha uma fonte de noticias para exibir no painel.</small>
</div>

<div class="form-group ">
    <label for="noticias">Notícias:</label>

    <select class="form-control" name="noticias" id="noticias" aria-describedby="noticiasHelp">
        <option @if($monitor->noticias == 1)selected @endif value="1">Ativo</option>
        <option @if($monitor->noticias == 0)selected @endif value="0">Desligado</option>
    </select>
    <small id="noticiasHelp" class="form-text text-muted icon-info"> Ative essa opção se desejar exibir notícias no monitor de separação.</small>
</div>
