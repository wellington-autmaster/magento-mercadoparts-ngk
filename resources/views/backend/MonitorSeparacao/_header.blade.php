<div class="alert " role="alert">
    <h4>Header do Monitor</h4>
    Ajuste a cor do background (fundo) e a cor do texto do header do monitor de senhas.
    <hr>

</div>
<div class="form-group">
    <label for="headerColor">Cor do background (fundo) Header</label>
    <input type="color" name="headerColor"  value="{{ $monitor->headerColor }}" class="form-control" id="headerColor" aria-describedby="headerColorHelp" >
    <small id="headerColorHelp" class="form-text text-muted icon-info"> Cor utilizada no background (Fundo) do header.</small>
</div>

<div class="form-group">
    <label for="headerTextColor">Cor do texto</label>
    <input type="color" name="headerTextColor"  value="{{ $monitor->headerTextColor }}" class="form-control" id="headerTextColor" aria-describedby="headerTextColorHelp" >
    <small id="headerTextColorHelp" class="form-text text-muted icon-info"> Cor utilizada no texto que exibe o nome da empresa, data, hora e informações climaticas </small>
</div>



