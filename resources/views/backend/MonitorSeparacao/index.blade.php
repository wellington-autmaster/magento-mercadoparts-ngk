@extends('backend.layouts.app')

@section ('title', 'Monitor de Separação')

@section('breadcrumb-links')

@endsection


@section('content')
<div class="" >
    <div id="app" class="card" >
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <h4 class="card-title mb-0">
                        Monitor de Separação
                        <small class="text-muted"> Configurações</small>

                    </h4>
                </div><!--col-->

            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <form id="frmMonitorSeparacao" action="{{route('admin.monitor.update')}}" method="post" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="header-tab" data-toggle="tab" href="#header" role="tab" aria-controls="header" aria-selected="true">Header</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="senhas-tab" data-toggle="tab" href="#senhas" role="tab" aria-controls="senhas" aria-selected="false">Senhas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="promocoes-tab" data-toggle="tab" href="#promocoes" role="tab" aria-controls="promocoes" aria-selected="false">Promoções</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="noticias-tab" data-toggle="tab" href="#noticias" role="tab" aria-controls="noticias" aria-selected="false">Notícias</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="header" role="tabpanel" aria-labelledby="header-tab"> @include('backend.MonitorSeparacao._header')</div>
                            <div class="tab-pane fade" id="senhas" role="tabpanel" aria-labelledby="senhas-tab">@include('backend.MonitorSeparacao._senhas')</div>
                            <div class="tab-pane fade" id="promocoes" role="tabpanel" aria-labelledby="promocoes-tab">@include('backend.MonitorSeparacao._promocoes')</div>
                            <div class="tab-pane fade" id="noticias" role="tabpanel" aria-labelledby="noticias-tab">@include('backend.MonitorSeparacao._noticias')</div>
                        </div>
                    </form>
                    <br>
                    <button type="submit" class="btn btn-success " form="frmMonitorSeparacao"> <i class="el-icon-success"> </i> Salvar</button>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
</div>

@endsection