<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('menus.backend.access.users.main') }}</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.auth.user.index') }}">Todos os usuários</a>
                <a class="dropdown-item" href="{{ route('admin.auth.user.create') }}">Criar novo</a>
                <a class="dropdown-item" href="{{ route('admin.auth.user.deactivated') }}">Desativados</a>
                <a class="dropdown-item" href="{{ route('admin.auth.user.deleted') }}">Deletados</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>