<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Avatar</th>
                <td><img src="{{ $user->picture }}" class="user-profile-image" /></td>
            </tr>

            <tr>
                <th>Nome</th>
                <td>{{ $user->name }}</td>
            </tr>

            <tr>
                <th>E-mail</th>
                <td>{{ $user->email }}</td>
            </tr>

            <tr>
                <th>Estado</th>
                <td>{!! $user->status_label !!}</td>
            </tr>

            <tr>
                <th>Confirmado</th>
                <td>{!! $user->confirmed_label !!}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->