<?php

namespace App\Jobs;

use App\Helpers\Backend\Marktplace\HelperMagento23;
use App\Models\Marktplace\Marktplace;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class jobAtualizaPrecoEstoque implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $produto, $empresa;

    public function __construct(Marktplace $empresa,$produto)
    {
        $this->produto = $produto;
        $this->empresa = $empresa;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $produto = new HelperMagento23();
        $produto->enviaProdutosMagento23($this->empresa,  $this->produto);
    }
}
