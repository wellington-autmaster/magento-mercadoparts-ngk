<?php

namespace App\Jobs;

use App\Helpers\Backend\Marktplace\HelperPluggTo;
use App\Models\Marktplace\Marktplace;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class JobFilaPluggto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $produto, $empresa;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Marktplace $empresa,$produto)
    {
        $this->produto = $produto;
        $this->empresa = $empresa;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $produto = new HelperPluggTo();
        $produto->enviaProdutosPlugto($this->empresa,  $this->produto);
    }

    public function tags()
    {
        return ['Plugg.To Produto', 'Emp:'.$this->empresa->empresa. ' - produto:'.$this->produto];
    }
}
