<?php

namespace App\Jobs;

use App\Models\Marktplace\MagentoPedidos;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class jobAtualizaPedidoMagento implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $entity_id;
    public $empresa;
    public  $novo_status;

    public function __construct($empresa, $entity_id, $novo_status)
    {
        $this->entity_id = $entity_id;
        $this->empresa = $empresa;
        $this->novo_status = $novo_status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $magento = new MagentoPedidos();
        $magento->atualizaStatusPedidoMagento($this->empresa, $this->entity_id, $this->novo_status);
    }
}
