<?php

namespace App\Jobs;

use App\Http\Controllers\Backend\Marktplace\OcPedidosController;
use App\Models\Marktplace\Marktplace;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportaPedidosOc implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected  $empresa,$pedido;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Marktplace $empresa,$pedido)
    {
        $this->pedido = $pedido;
        $this->empresa = $empresa;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $imped = new OcPedidosController();
        $imped = $imped->importaPedidosSSPlus($this->empresa,$this->pedido);
    }
}
