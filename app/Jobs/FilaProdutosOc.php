<?php

namespace App\Jobs;


use App\Events\Backend\Opencart\EventAtualizaProdutoOpencart;
use App\Models\Marktplace\Marktplace;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class FilaProdutosOc implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $produto, $empresa;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Marktplace $empresa, $produto)
    {
        $this->produto = $produto;
        $this->empresa = $empresa;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        event(new EventAtualizaProdutoOpencart($this->empresa,  $this->produto));
       /*if($this->produto <> '9999999'){

       }else{
           if($this->produto =='9999999'){
               $users = User::all();
               Notification::send( $users, new UpdateProdutos('Todos os produtos de e-commerce foram atualizados para empresa '. $this->empresa->empresa));
           }
       }*/

    }

    public function tags()
    {
        return [$this->empresa->empresa.  ' Opencart ' .  ' - produto:'.$this->produto];
    }
}
