<?php

namespace App\Models\Monitor;

use Illuminate\Database\Eloquent\Model;


class MonitorSeparacao extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'monitorseparacao';
    protected $primaryKey = 'codigo';
   // public $incrementing = false;



    public static function Senhas(){
        $senhas =  MonitorSeparacao::where('hora_fim', '>', '00:00:00')
            ->where('modoentregabalcao',3)->get();
        
            return $senhas;
    }

    public static function Senhas2(){
        $senhas =  MonitorSeparacao::where('modoentregabalcao',3)->get();
        
            return $senhas;
    }



}
