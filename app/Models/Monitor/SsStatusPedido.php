<?php

namespace App\Models\Monitor;


use App\Events\EventMonitorSeparacaoSenha;
use App\Events\EventSsStatusPedidos;
use App\Events\EventTotalPedido;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Null_;
use PhpParser\Node\Expr\Cast\Object_;

/**
 * @method static whereDate(string $string, string $string1)
 */
class SsStatusPedido extends Model
{


    protected $fillable = [
/*        'empresa' ,
        'pedido' ,
        'codigo_cliente' ,
        'codigo_status',
        'status' ,
        'data_pedido',
        'data_inicial' ,
        'data_final' ,
        'vendedor' ,
        'operador' ,
        'modoentrega',*/
        'empresa',
        'pedido',
        'codigo_cliente',
        'data_pedido',
        'modoentrega',
        'status',
        'a0001',
        's0001',
        's0002',
        'c0002',
        'e0001',
        'e0002',
        'e0003',
        'D0001',
        'entregador_codigo',
        'entregador_nome',
        'conferente_separador',
        'nome_conferente_separador',
        'finalizado',
        'cancelado',
    ];


    public static function gravaPedido($objJson)
    {

        //Verifica se existem atribustos com o separador
        if($objJson->codstatus == 'S0001' || $objJson->codstatus == 'S0002'){ // Se o status for Pedido em separação ou Separação Concluida

            if(!isset($objJson->{'nome-conferente-separador'}) || !isset($objJson->{'conferente-separador'}) ){  //Verifica se os registros estão em branco

                $objJson->nome_separador   =  '';
                $objJson->codigo_separador =  '';
            }else{ // Se existir registros, ele repassa o nome para as variaveis

                $objJson->nome_separador = $objJson->{'nome-conferente-separador'} ;
                $objJson->codigo_separador = $objJson->{'conferente-separador'} ;
            };
        }



        //Verifica se existem atribustos com o Conferente
        if($objJson->codstatus == 'C0001' || $objJson->codstatus == 'C0002'){ // Se o status for Pedido em separação ou Separação Concluida
            if(!isset($objJson->{'nome-conferente-separador'}) || !isset($objJson->{'conferente-separador'}) ){  //Verifica se os registros estão em branco
                $objJson->nome_conferente   =  ' '; ;
                $objJson->codigo_conferente =  ' ';
            }else{ // Se existir registros, ele repassa o nome para as variaveis
                $objJson->nome_conferente = $objJson->{'nome-conferente-separador'} ;
                $objJson->codigo_conferente = $objJson->{'conferente-separador'} ;
            };
        }


        //Ajusta entregador
        if(!isset($objJson->{'entregador-nome'}) || !isset($objJson->{'entregador-codigo'}) ){
            $objJson->{'entregador-nome'} = ' ';
            $objJson->{'entregador-codigo'} = ' ';
        };



        //Verifica se retornou dados de modo entrega balcao
        if(!isset($objJson->modoentregabalcao)){
            $objJson->modoentregabalcao = '3';
        }

        //Indica se o pedido esta concluido ou não
       // if(($objJson->codstatus == 'C0002' && $objJson->modoentregabalcao == '3') || $objJson->codstatus == 'E0003' || $objJson->codstatus == 'D0001'){
        if($objJson->codstatus == 'E0003' || $objJson->codstatus == 'D0001'){
            $objJson->finalizado = true; // indica se o pedido concluiu todos os status
        }else{
            $objJson->finalizado = false; // indica se o pedido concluiu todos os status
        }


        //Verifica se o pedido existe, faz o update de dados ou cria um novo registro
        $sspedido = SsStatusPedido::where('empresa', $objJson->empfil)
            ->where('pedido', $objJson->nronff)
                ->first();



        if ($sspedido) {
            // O Pedido ja esta cadastrado

            if($objJson->codstatus == 'E0002' && $sspedido->E0002 <> null){ //Se vir o status Entrega em transito e esse registro existir no banco, ele não será mudado.

            }else{
                if($objJson->codstatus == 'C0001' && $sspedido->C0001 <> null){ // se vir status Pedido em conferencia e o mesmo ja existir no banco, não acontece nada

                }else{
                    $sspedido->{$objJson->codstatus} = Carbon::now()->format("Y-m-d H:i:s"); // Cadastra uma nova data caso as condições a cima não sejam atendidas
                }
            }
            $sspedido->status = $objJson->codstatus;
            $sspedido->entregador_codigo = $objJson->{'entregador-codigo'};
            $sspedido->entregador_nome = $objJson->{'entregador-nome'};
            $sspedido->finalizado = $objJson->finalizado;

            //Verifica se ja existe registros de conferete e separador
            //Verifica se é separação
            if($objJson->codstatus == 'S0001' || $objJson->codstatus == 'S0002'){
                $sspedido->codigo_separador = $objJson->codigo_separador;
                $sspedido->nome_separador = $objJson->nome_separador;
            }

            if($objJson->codstatus == 'C0001' || $objJson->codstatus == 'C0002'){
                $sspedido->codigo_conferente = $objJson->codigo_conferente;
                $sspedido->nome_conferente = $objJson->nome_conferente;
            }

          //  $sspedido->save();

        } else {
            // O pedido não estava cadastrado
            $sspedido = new self();
            $sspedido->empresa = $objJson->empfil;
            $sspedido->pedido = $objJson->nronff;
            $sspedido->codigo_cliente = $objJson->clifor;
            $sspedido->descricao_cliente = $objJson->descli;
            $sspedido->data_pedido = Carbon::now()->format("Y-m-d H:i:s");
            $sspedido->modoentrega = $objJson->modoentregabalcao;
            $sspedido->status = $objJson->codstatus;
            $sspedido->{$objJson->codstatus} = Carbon::now()->format("Y-m-d H:i:s");
            $sspedido->entregador_codigo = $objJson->{'entregador-codigo'};
            $sspedido->entregador_nome = $objJson->{'entregador-nome'};


            $sspedido->finalizado = $objJson->finalizado;
            $sspedido->cancelado = false;

            //Verifica se é separação
            if($objJson->codstatus === 'S0001' || $objJson->codstatus === 'S0002'){
                $sspedido->codigo_separador = $objJson->codigo_separador;
                $sspedido->nome_separador = $objJson->nome_separador;
            }

            if($objJson->codstatus === 'C0001' || $objJson->codstatus === 'C0002'){
                $sspedido->codigo_conferente = $objJson->codigo_conferente;
                $sspedido->nome_conferente = $objJson->nome_conferente;
            }

            //Verifica se é conferencia

         //   $sspedido->save();

        }

        //Ajusta descrição dos status
        $sspedido =  self::getDescricaoStatus($sspedido);
        $sspedido->save();

        $sspedido = self::getTempoProcessos($sspedido);

        self::getTotais($sspedido->empresa);
        self::notifica($sspedido->empresa);

    }

    public static function notifica($empresa)

    {
       $pedidos = self::whereDate('data_pedido', now()->format("Y-m-d"))->where('empresa', $empresa)->get();

       foreach($pedidos as $pedido) {

           //Ajusta nome do colaborador
           $s = $pedido->status;

           if( $s === 'E0001' || $s === 'E0002' || $s === 'E0003'){
               $pedido->colaborador = $pedido->entregador_nome;
           }elseif($s === 'S0001' || $s === 'S0002' ) { // Pedido em separação ou separação concluida
               $pedido->colaborador =  $pedido->nome_separador ;
           }elseif($s === 'C0001' || $s === 'C0002' ) { // Pedido em conferencia ou conferencia do pedido concluida
               $pedido->colaborador = $pedido->nome_conferente;
           }

           //Verifica tempo limite
           $pedido = self::getTempoProcessos($pedido);

           //Enviar somente o primeiro nome
           $pedido->colaborador = explode(' ',$pedido->colaborador)[0];

           $arr_pedido[] = [
               'pedido' => $pedido->pedido,
               'codigo_cliente' => $pedido->codigo_cliente,
               'descricao_cliente' => $pedido->descricao_cliente,
               'data_pedido' => $pedido->data_pedido,
               'modoentrega' => $pedido->modoentrega,
               'status' => $pedido->status,
               'status_descricao' => $pedido->status_descricao,
               'data_status' => $pedido->{$pedido->status},
               'colaborador' => $pedido->colaborador,
               'finalizado' => $pedido->finalizado,
               'cancelado' => $pedido->cancelado,
               'tempo' => $pedido->tempo,
           ];

       }

        broadcast(new EventSsStatusPedidos($arr_pedido, $empresa)) ;

    }

    public static function ajustaStatus($objJson)
    {
        // O Status precisa ser diferente de A0001 Pedido efetuado aguardando separação e conferencia

        if($objJson->codstatus <> 'A0001') {

            switch ($objJson->codstatus) {
                case 'S0001' :
                    $objJson->oldStatus = 'A0001';
                    break;
                case 'S0002' :
                    $objJson->oldStatus ='S0001';
                    break;
                case 'C0001' :
                    $objJson->oldStatus = 'S0002';
                    break;

                case 'C0002' :
                   // $objJson->oldStatus = 'S0002';
                    $objJson->oldStatus = 'C0001';
                    break;
                case 'E0001' :
                    $objJson->oldStatus = 'C0002';
                    break;
                case 'E0002' :
                    $objJson->oldStatus = 'C0002'; //testando
                    break;
                case 'E0003' :
                    $objJson->oldStatus = 'E0002';
                    break;
                case 'D0001' :
                    // deletar os pedidos
                    break;
            }
/*
            $pedido_anterior = SsStatusPedido::where('empresa', $objJson->empfil)
                ->where('pedido', $objJson->nronff)
                ->where('codigo_status', $objJson->oldStatus)->first();
            if($pedido_anterior){
                $pedido_anterior->data_final = Carbon::now()->format("Y-m-d H:i:s");
                $pedido_anterior->save();
            }*/

        }
    }

    public static function getDescricaoStatus($pedido)
    {

        // Em caso de alteração neste bloco, verificar o FrtMonitorStatusController para manter identico
        switch ($pedido->status) {
            case 'A0001' :  $pedido->status_descricao = 'Aguardando separação'; break;
            case 'S0001' :  $pedido->status_descricao = 'Separando'; break;
            case 'S0002' :  $pedido->status_descricao = 'Aguardando conferência'; break;
            case 'C0001' :  $pedido->status_descricao = 'Em Conferência'; break;
            case 'C0002' :  $pedido->status_descricao = 'Aguardando liberar entrega'; break;
            case 'E0001' :  $pedido->status_descricao = 'Enrega liberada'; break;
            case 'E0002' :  $pedido->status_descricao = 'Saiu para entrega'; break;
            case 'E0003' :  $pedido->status_descricao = 'Entrega realizada'; break;
            case 'D0001' :  $pedido->status_descricao = 'Cancelado'; break;
        }

        return $pedido;
    }

    public static function getTempoProcessos($pedido)
    {

       $tempo =  DB::connection('pgsql')->table('tempo_por_processo_view')->where('empresa',$pedido->empresa)->get();
        $tempo = $tempo[0];

        switch ($pedido->status) {
            case 'A0001' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->separacao_tempo_maximo_espera); break; //Aguardando separação
            case 'S0001' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->separacao_tempo_maximo_execucao); break;            // Separando
            case 'S0002' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->conferencia_tempo_maximo_espera); break; //Aguardando conferência
            case 'C0001' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->conferencia_tempo_maximo_execucao); break;           //Em Conferência
            case 'C0002' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->entrega_tempo_maximo_espera); break; //Aguardando liberar entrega
            case 'E0001' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->entrega_tempo_maximo_espera); break;      //Enrega liberada
            case 'E0002' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->entrega_tempo_maximo_execucao); break;        //Saiu para entrega'
            case 'E0003' :  $pedido->tempo = SsStatusPedido::transHoraMinuto($tempo->entrega_tempo_maximo_execucao); break;        //Entrega realizada
            case 'D0001' :  $pedido->tempo = SsStatusPedido::transHoraMinuto('00:00:00'); break;            //Cancelado
        }

        return $pedido;
    }

    public static function transHoraMinuto($tempo)
    {
        $tempo = explode(':', $tempo);

        $hora_em_minuto = $tempo[0] * 60;
        $minutos = $tempo[1] + $hora_em_minuto;
        $minutos = $minutos . '.' . $tempo[2];

        return $minutos;
    }

    public static function ajustaCronometro($objJson)
    {
        /****** $objJson->datastatus ******
         * Status que deverá inicializar cronometro
        1	A0001	Pedido efetuado aguardando separação e conferencia	SIM
        3	C0002	Conferencia do Pedido concluída	SIM
        5	S0002	Separação do pedido concluída	SIM

        $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
         * */

        switch ($objJson->codstatus) {
            case 'A0001' :   //Pedido efetuado aguardando separação e conferencia - Zera cronometro
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Aguardando separação';
                $objJson->colaborador ='';
                break;
            case 'S0001' :  //Pedido em Separação
              //  $objJson->datastatus = SsStatusPedido::cronometroAnterior($objJson->empfil, $objJson->nronff, 'A0001');
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Separação';
                $objJson->colaborador = $objJson->{'nome-conferente-separador'};
                break;
            case 'S0002' : //Separação do pedido concluída
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Aguardando conferência';
                $objJson->colaborador = $objJson->{'nome-conferente-separador'};
                break;
            case 'C0001' : //Pedido em Conferencia
              //  $objJson->datastatus = SsStatusPedido::cronometroAnterior($objJson->empfil, $objJson->nronff, 'S0002');
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Conferência';
                $objJson->colaborador = $objJson->{'nome-conferente-separador'};
                break;
            case 'C0002' : //Conferencia do Pedido concluída
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Aguardando liberação para entrega';
                $objJson->colaborador = '';
                break;
            case 'E0001' : //Entrega Liberada (definido o motoboy)
                $objJson->datastatus = SsStatusPedido::cronometroAnterior($objJson->empfil, $objJson->nronff, 'C0002');
                $objJson->descstatusres = 'Enrega liberada';
                $objJson->modoentregabalcao = '2';
                $objJson->colaborador =  $objJson->{'entregador-nome'};
                break;
            case 'E0002' : //Entrega em Transito (Saiu para Entregar)
            //    $objJson->datastatus = SsStatusPedido::cronometroAnterior($objJson->empfil, $objJson->nronff, 'C0002');
                $objJson->datastatus = Carbon::now()->format("Y-m-d H:i:s");
                $objJson->descstatusres = 'Saiu para entrega';
                $objJson->modoentregabalcao = '2';
                $objJson->colaborador =  $objJson->{'entregador-nome'};
                break;
            case 'E0003' : //'Entrega Fechada (Motoboy ja voltou)
                $objJson->datastatus = SsStatusPedido::cronometroAnterior($objJson->empfil, $objJson->nronff, 'C0002');
                $objJson->descstatusres = 'Concluído';
                $objJson->modoentregabalcao = '2';
                $objJson->colaborador =  $objJson->{'entregador-nome'};
                break;
            case 'D0001' :
                // deletar os pedidos
                break;
        }
        //Mostra apenas primeiro nome do colaborador
        $objJson->colaborador = explode(' ', $objJson->colaborador)[0];

        return $objJson;

    }

    public static function cronometroAnterior($empfil, $nronff, $status )
    {
        $data_inicial = SsStatusPedido::where('empresa', $empfil)
            ->where('pedido', $nronff)
            ->where('status', $status)->first();
         //   ->where('codigo_status', $status)->first();

        if($data_inicial){
            $data_inicial = $data_inicial->data_inicial;
            Log::info('tem data inicial');
        }else{
            $data_inicial =  Carbon::now()->format("Y-m-d H:i:s");
            Log::info('sem data inicial');
        }

        return $data_inicial;
    }

    public static function getTotalStatus($codigo_status, $modoentrega)
    {
        return SsStatusPedido::whereIn('status', $codigo_status)
            ->whereIn('modoentrega', $modoentrega)
            ->where('updated_at','>=',Carbon::now()->format("Y-m-d"))
            ->where('cancelado',false)->count();
    }

    public static function getTotais($empresa)
    {
        $periodo = Carbon::now()->format("Y-m-d");

        $total_pedidos = SsStatusPedido::where('status', 'A0001')
            ->where('updated_at','>=',$periodo)
                ->where('cancelado',false)
                    ->where('finalizado', false)
                        ->count();

        $total_separacao = SsStatusPedido::where('status', 'S0001')
            ->where('updated_at','>=',$periodo)
                 ->where('cancelado',false)
                      ->where('finalizado', false)
                          ->count();

        $total_conferencia = SsStatusPedido::wherein('status', ['S0002','C0001'])
            ->where('updated_at','>=',$periodo)
                 ->where('cancelado',false)
                     ->where('finalizado', false)
                         ->count();

        $total_aguarda_motoboy = SsStatusPedido::wherein('status', ['C0002','E0001','E0002'])
            ->where('modoentrega', 2)
                ->where('updated_at','>=',$periodo)
                     ->where('cancelado',false)
                        ->where('finalizado', false)
                             ->count();
        $total_aguarda_balcao = SsStatusPedido::wherein('status', ['C0002','E0001','E0002'])
            ->where('modoentrega', 3)
                 ->where('updated_at','>=',$periodo)
                    ->where('cancelado',false)
                         ->where('finalizado', false)
                              ->count();

        $total_motoboy =  SsStatusPedido::where('status', 'E0003')
            ->where('modoentrega', 2)
                ->where('updated_at','>=',$periodo)
                    ->where('cancelado',false)
                        ->where('finalizado', true)
                            ->count();

        $total_balcao =  SsStatusPedido::where('modoentrega', 3)
            ->where('updated_at','>=',$periodo)
                 ->where('cancelado',false)
                    ->where('finalizado', true)
                        ->count();

        $totalpedidos =
            [
                'pedidos' => $total_pedidos,
                'separacao' => $total_separacao,
                'conferencia' => $total_conferencia,
                'aguardandoentregabalcao' => $total_aguarda_balcao,
                'aguardandoentregamotoboy' => $total_aguarda_motoboy,
                'motoboy' => $total_motoboy,
                'balcao' => $total_balcao,
            ];

        Log::info('totais', $totalpedidos);

        broadcast(new  EventTotalPedido($totalpedidos, $empresa));

        return $totalpedidos;

    }
}
