<?php

namespace App\Models\Monitor;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model
{
    
    public function Empresa()
    {
        return $this->hasOne('App\Models\Empresa\Empresa','id','empresa_id');
    }

}
