<?php

namespace App\Models\Monitor;

use Illuminate\Database\Eloquent\Model;

class MonitorPromocao extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'view_aut_promocao';
    protected $primaryKey = 'codigopromocao';


    public static function getPromocao()
    {
        return MonitorPromocao::where('ativa',true)->limit(50)->orderBy('dtfinal','asc')->get();
    }
}
