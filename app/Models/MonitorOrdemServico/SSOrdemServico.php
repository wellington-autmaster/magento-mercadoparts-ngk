<?php

namespace App\Models\MonitorOrdemServico;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;


class SSOrdemServico extends Model
{
    //Model de Ordem de Serviço
    protected $connection = 'pgsql';
    protected $table = 'pcmease0';


public static function OS($empresa)
{

    $dias_retroativos =  env('SS_DIAS_OS_FECHADA');

   $os =  SSOrdemServico::where('empfil', '=', $empresa)->whereNotIn('status', ['C','F','B'])
        ->orWhere(function ($query) use ($dias_retroativos) {
            $query->where('dtaber', '>=', Carbon::now()->subDay($dias_retroativos))
            ->where('status', '=', 'B');
        })
       ->orWhere(function ($query) {
           $query->where('dtaber', '=', new DateTime)
               ->where('status', '=', 'F');
       })
       ->orderBy('status','asc','dtaber','desc')
        ->get();

   return $os;

//dtfatur ->whereBetween('votes', [1, 100])->get();
}

}
