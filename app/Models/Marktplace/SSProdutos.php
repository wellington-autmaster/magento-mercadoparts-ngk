<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class SSProdutos extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'pccdite0';
    //protected $primaryKey = 'codigo';


    public function similares()
    {
       return $this->hasMany('App\Models\Marktplace\SSProdutos','similarm','similarm');

    }


    public function estoque()
    {
        return $this->hasOne('App\Models\Marktplace\SSEstoque', 'item','codigo');
    }


}
