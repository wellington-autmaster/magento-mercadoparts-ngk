<?php

namespace App\Models\Marktplace;

use App\Models\Empresa\Configuracao;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Marktplace extends Model
{
    public  static function Empresas($integracao)
    {
        return Marktplace::where('status', true)->where('codigo', $integracao)->get();
    }









    public static function OpencartToken($empresa)
    {
        // Se o valor do token não estiver armazenado em cache, será gerado um novo token
        if (!Cache::has('oc_key_'.$empresa)) {
            $opencart = Marktplace::where('nome','opencart')->where('empresa', $empresa)->first();

            $http = new Client();

            $response = $http->post(parse_url($opencart->url, PHP_URL_SCHEME).'://api.'. parse_url($opencart->url,PHP_URL_HOST).'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $opencart->client_id,
                    'client_secret' => $opencart->client_secret,
                    'scope' => '*',
                ],
            ]);

            $api_token = json_decode((string) $response->getBody());
            $api_token  = $api_token->access_token;
           // $api_token  = $api_token->token_type. ' '.$api_token->access_token;


            Cache::put('oc_key_'.$empresa, $api_token, now()->addDays(1));

        }

        return Cache::get('oc_key_' . $empresa);
    }




    /**
     * @param $i 0 = Atualização completa de produtos. 1 = atualização diaria, 2= atualização individual do produto
     * @param $produto - Caso for atualização individual, irá atualizar com esse produto
     * @param $empresa - Recebe a empresa que será atualizada (0001, 0002)
     * @return array
     */
    public static function TipoAtualizacao($i, $produto, $empresa, $marktplace )
    {
        /**Recebe o valor inteiro para identificar o tipo de atualização, depois retorna um array com os dados.
         *  Caso a atualização for diaria, o sistema verifica antes se é para atualizar tudo, dependendo da
         * configuração
         */

            //Quando for atualzação diaria, verifica antes se é atualização geral
            if($i == 1){
                $configuracao = Configuracao::where('modulo', 'Marktplace')
                    ->where('empresa',$empresa)
                    ->where('configuracao', $marktplace)->first();

                     //Se atualizacao_geral for t (Verdadeira) então atualiza todos os produtos.
                    if($configuracao->valor == 'T'){
                    $i = 0;

                    //Volta o valor da configuração ao normal
                        $configuracao->valor = 'F';
                        $configuracao->save();
                }

            }

        //Tipo de atualização
        switch ($i) {
            case 0:
                //0 - Atualização Completa;
                $coluna= 'data_atualizacao';
                $operador = '>';
                $valor = date('2000-01-01');

                break;
            case 1:
                //1 - Atualzação diaria
                $coluna= 'data_atualizacao';
                $operador = '=';
                $valor = date('Y-m-d');
                break;
            case 2:
               //2 - Atualização de produto
                $coluna= 'codigo_interno';
                $operador='=';
                $valor = $produto;
                break;
        }

        $dados = ['coluna' => $coluna , 'operador'=>$operador, 'valor'=>$valor];

        return $dados;

    }


}
