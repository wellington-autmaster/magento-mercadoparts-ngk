<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class ProdutoCaracteristica extends Model
{
    public function caracteristica()
    {
        return $this->hasOne('App\Models\Marktplace\Caracteristica','id','caracteristica_id');
    }

    static function produtoCarateristica($codigo)
    {
        $arrCaracteristicas = array();
        $caracteristicas = ProdutoCaracteristica::where('ssproduto_id', $codigo)->get();
            foreach($caracteristicas as $caracteristica){            
                if(isset($caracteristica->valor)){
                    $arrCaracteristicas[] = [
            
                        'id_descricao'  => $caracteristica->caracteristica_id,
                        'descricao'     => $caracteristica->caracteristica->descricao,
                        'valor_id'      => $caracteristica->id,
                        'valor'         => $caracteristica->valor
                    
                    ]; 
                    
                }
            }
                    
        return $arrCaracteristicas; 
        
                    
    }
}
