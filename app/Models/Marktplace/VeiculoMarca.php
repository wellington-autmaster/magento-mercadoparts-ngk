<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class VeiculoMarca extends Model
{
    protected $fillable =
    [
        'veiculo_tipo_id',
        'name'          ,
        'fipe_name'     ,
        'order'         ,
        'key'           ,
        'id_fipe'       ,
    ];

    public function veiculos()
    {
        return $this->hasMany('App\Models\Marktplace\Veiculo');
    }

}
