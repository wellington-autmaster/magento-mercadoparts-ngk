<?php

namespace App\Listeners\Backend\Opencart;

use App\Helpers\Backend\Marktplace\HelperOpencart;
use App\Http\Controllers\Backend\Marktplace\OpencartController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnviarProduto
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $produto = new OpencartController();
        $produto->enviaProdutosOpencart($event->empresa,  $event->produto);
    }
}
