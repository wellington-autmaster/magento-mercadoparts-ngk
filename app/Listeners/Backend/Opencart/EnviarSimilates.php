<?php

namespace App\Listeners\Backend\Opencart;

use App\Helpers\Backend\Marktplace\HelperOpencart;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class EnviarSimilates
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {


    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $produto = new HelperOpencart();
        $produto->enviaSimilares($event->empresa,  $event->produto);
    }
}
