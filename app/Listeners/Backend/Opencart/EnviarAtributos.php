<?php

namespace App\Listeners\Backend\Opencart;

use App\Http\Controllers\Backend\Marktplace\OpencartController;


class EnviarAtributos
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $produto = new OpencartController();
        $produto->enviarAtributos($event->empresa,  $event->produto);
    }
}
