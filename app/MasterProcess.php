<?php

namespace App;

use App\Models\Empresa\Empresa;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MasterProcess extends Model
{

    public static function ativaContrato($empresa, $modulo, $status = true, $producao= true, $teste=0)
    {
        $array = [
            'cnpj' => $empresa->cgc,
            'modulo' => $modulo,
            'status' => $status,
            'producao' => $producao,
            'dias_teste' => $teste,
            'cancelado' => false
        ];
        return MasterProcess::enviaInformacoes($array);
    }

    public static function enviaInformacoes($informacoes)
    {
        $token = MasterProcess::tokenDashboard();

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => $token
        ];

        $http = new Client();
        $response = $http->post(config('estatisticas.uri').'/api/contrato',[
            'headers' => [
                'accept' => 'application/json',
                'authorization' => $token
            ],
            'form_params' => $informacoes
        ]);

        if( $response->getStatusCode() === 200 ) {
            $status = $informacoes;
        }else {
            $status = $response->getBody();
        }

        return $response->getBody() ;
    }

    public static function getContrato($empresa, $modulo)
    {
        $token = self::tokenDashboard();

        $http = new Client();
        $response = $http->post(config('masterprocess.uri').'/api/contrato/get',[
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'accept' => 'application/json',
                'authorization' => $token
            ],
            'form_params' => [
                'cnpj' => $empresa->cgc,
                'modulo' => $modulo
                ]
        ]);

        return  $response->getBody();
    }

    public static function statusContrato($marktplace, $modulo)
    {
        $empresa = Empresa::where('codigo', $marktplace->empresa)->first();

      $contrato =  Cache::remember('contrato-'.$modulo, 60, function () use ($empresa, $modulo) {
          $valor = self::getContrato($empresa, $modulo);
          return json_decode($valor);
        });
      return $contrato;
    }

    public static function tokenDashboard()
    {
        $http = new Client();
        $response = $http->post(config('masterprocess.uri').'/oauth/token', [
            'form_params' => config('masterprocess.api')
        ]);
        $token = json_decode((string) $response->getBody(), true);

        $token =  $token['token_type'].' '.$token['access_token']  ;

        return $token;
    }



}
