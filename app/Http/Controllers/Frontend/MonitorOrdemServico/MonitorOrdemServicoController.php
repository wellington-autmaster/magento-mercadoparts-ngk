<?php

namespace App\Http\Controllers\Frontend\MonitorOrdemServico;

use App\Events\EventMonitorOrdemServico;

use App\Models\MonitorOrdemServico\SSOrdemServico;
use App\Http\Controllers\Controller;
use App\Models\MonitorOrdemServico\SSProdutivo;
use App\Models\SSClienteFornecedor;
use Illuminate\Http\Request;

class MonitorOrdemServicoController extends Controller
{
    public function OrdenServicos($empresa)
    {
        /** bg-warning -- Amarelo
         * bg-danger - Vermelho
         * bg-success - Verde
         */
        // Coloca zeros a esquerda do codigo da emrpesa
        $empresa = str_pad( $empresa, 4, '0', STR_PAD_LEFT );


        return view('frontend.layouts.monitorservico', compact('empresa'));
    }

    public function getOrdens($empresa)
    {
        $ordens = SSOrdemServico::OS($empresa);

        $arrOrdens = [];

        foreach ($ordens as $ordem)
        {
            $produtivo = SSProdutivo::getProdutivo($ordem->empfil, $ordem->ordser);
            if($produtivo != null){
                $produtivo = SSClienteFornecedor::getPessoa($produtivo->mecani);
                $produtivo = $produtivo->descri;
            }else{
                $produtivo = ' ';
            }



            switch ($ordem->status){
                case ' ' :{
                    $ordem->status = 'ABERTA';
                    $ordem->class = 'bg-info';
                    break;
                }
                case  'B' :{
                    $ordem->status = 'FECHADA';
                    $ordem->class = 'bg-secondary';
                    break;
                }
                case 'F' : {
                    $ordem->status = 'FATURADA';
                    $ordem->class = 'bg-success';
                    break;
                }
            }

            array_push($arrOrdens,[
                'ordem' => $ordem->ordser,
                'status' => $ordem->status,
                'veiculo' => $ordem->desvei,
                'cliente' => $ordem->descri,
                'mecanico' => $produtivo,
                'class' => $ordem->class,
                'abertura' => date('d/m/Y', strtotime($ordem->dtaber)),
            ]);
        }

        $arr = ['ordens' => $arrOrdens, 'total' => $ordens->count()];


        return response($arr,200);
    }
}
