<?php

namespace App\Http\Controllers\Frontend\MonitorSeparacao;

use App\Events\EventMonitorSeparacaoSenha;
use App\Http\Controllers\Controller;
use App\Models\Monitor\MonitorChamada;
use App\Models\Monitor\MonitorPromocao;
use App\Models\Monitor\Monitor;
use App\Models\Monitor\MonitorSeparacao;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \PDO;


class FrtMonitorSeparacaoController extends Controller
{
    /**
     * @return Senhas() Retorna as ultimas 10 senhas da separação
     */
    public function senhas()
    {
        $senhas = MonitorChamada::where('empresa', '0001')->orderBy('codigo', 'DESC')->get();
        return response($senhas,200);
    }


    /**
     * @param $empresa_id
     * @return Retorna o monitor correspondente ao ID da empresa.
     */
    public function monitor($empresa_id)
    {
       // $chamadas = MonitorChamada::all();
        $monitor  = Monitor::where('empresa_id',$empresa_id)->first();
        return view('frontend.monitor', compact('monitor'));
    }

    /**
     * @return MonitorPromocao[]: Envia as promoções via broadcast para o pusher, que será recebido por um canal no monitor
     * Pesquisa um monitor qualquer para buscar um canal de promoções (São todos iguais), depois pesquisa as promoções.
     * Foi criado uma view no postgres para buscar as informações
     */
    public static function promocoes()
    {
      //  $monitor = Monitor::where('status',1)->first();
        // Pesquisa os monitores ativos de cada empresa
        $promocoes = MonitorPromocao::getPromocao();
       //     broadcast(new EventPromocoes($promocoes,$monitor->canalPromocao)); -- Faz a comunicação sem o pusheer, direto no componente
        return $promocoes;
    }


    /**
     * @return true:
     * Pesquisa os monitores de todas as empresas,
     * Caso a senha ainda nao estiver cadastrada no mysql é pq ela ainda não foi emitida, se for o caso
     * é gravado no banco mysql e enviado um sinal para o pusher com as informações da senha, o monitor fica com um canal
     * conectado.
     *
     * Esse metodo é disparado 2x a cada minito por um schedule.
     *
     */
    public static function verificaSenha()
    {

        // Pesquisa os monitores ativos de cada empresa
        $monitores = Monitor::where('status',1)->get();

        foreach($monitores as $monitor)
        {

            // Se true, exibo entregas do balcão, senão exibe todas.
            if ( $monitor->modoentregabalcao){
                $modoentrega = ['3'];
            }else{
                $modoentrega = ['0','1','2','3'];
            }
            // Pesquisa as senhas vinculadas a empresa de cada monitor
            $SSseparacoes = MonitorSeparacao::where('empfil', $monitor->empresa)
                ->whereIn('modoentregabalcao', $modoentrega)
                ->orWhereNull('modoentregabalcao')
                    ->distinct('codigo')
                        ->limit(10)
                            ->orderBy('codigo','DESC')
                                ->get();



            foreach($SSseparacoes as $SSseparacao)
            {

                if ($SSseparacao->clifor == '') {
                    continue;
                }

                //Ajusta hora fim vinda do SSPlus
                if ($SSseparacao->hora_fim == ''){
                    $SSseparacao->hora_fim = '00:00:00';
                }

                //Verifica o cliente de cada separação, se for 35 pesquisa o nome difitado na tabela pccmorc0
                if($SSseparacao->clifor == '0000035'){
                    $cliente35 = DB::connection('pgsql')->table('pccmorc0')->where('pedido', $SSseparacao->nronff )->first();
                    $SSseparacao->descri = $cliente35->descri;
                }

                if($SSseparacao->modoentregabalcao == NULL){
                    $SSseparacao->modoentregabalcao = '3';
                }

                //Pesquisa a senha na platadorma;
                $PlataformaSenha = MonitorChamada::where('empresa', $monitor->empresa)->where('codigo', $SSseparacao->codigo)->get();

                if($PlataformaSenha->count() <= 0)
                {

                    //Se não existir senha na platadorma ela será criada.
                    $newSenha = new MonitorChamada();
                    $newSenha->empresa = $SSseparacao->empfil;
                    $newSenha->planil = $SSseparacao->planil;
                    $newSenha->data = $SSseparacao->data;
                    $newSenha->hora_inicio = $SSseparacao->hora_inicio;
                    $newSenha->hora_fim = $SSseparacao->hora_fim;
                    $newSenha->codigo = $SSseparacao->codigo;
                    $newSenha->clifor = $SSseparacao->clifor;
                    $newSenha->descri = $SSseparacao->descri;
                    $newSenha->nronff = $SSseparacao->nronff;
                    $newSenha->modoentregabalcao = $SSseparacao->modoentregabalcao;
                    $newSenha->save();
                    //Dispara o evento de chamada de senha
                    broadcast(new EventMonitorSeparacaoSenha($newSenha,$monitor->canal));



                }   else
                {

                    // Se a senha ja existir na plataforma e não for finalizada no ss nada acontece, senão
                    // é verificado se a senha na plataforma
                    if($SSseparacao->hora_fim <> '00:00:00'){

                        if($PlataformaSenha[0]->hora_fim == '00:00:00' ){

                            $PlataformaSenha[0]->delete();
                            $newSenha = new MonitorChamada();
                            $newSenha->empresa = $SSseparacao->empfil;
                            $newSenha->planil = $SSseparacao->planil;
                            $newSenha->data = $SSseparacao->data;
                            $newSenha->hora_inicio = $SSseparacao->hora_inicio;
                            $newSenha->hora_fim = $SSseparacao->hora_fim;
                            $newSenha->codigo = $SSseparacao->codigo;
                            $newSenha->clifor = $SSseparacao->clifor;
                            $newSenha->descri = $SSseparacao->descri;
                            $newSenha->nronff = $SSseparacao->nronff;
                            $newSenha->modoentregabalcao = $SSseparacao->modoentregabalcao;
                            $newSenha->save();
                            //Dispara o evento de chamada de senha
                            broadcast(new EventMonitorSeparacaoSenha($newSenha,$monitor->canal));

                        }
                    }
                }
            }
        }
        return 'ué';
    }

    public static function pg_notify_monitor(){
        set_time_limit(0);
        $execs =  DB::connection('pgsql');
        $execs->select('LISTEN ntfy_pccmces0');
        while (1) {
            $result= $execs->getPdo()->pgsqlGetNotify(PDO::FETCH_ASSOC, 1000);
            if ( $result ) {
              Log::info(json_encode($result));
            };
            flush();
        }
    }


}
