<?php

namespace App\Http\Controllers\Frontend\MonitorStatusPedido;

use App\Http\Controllers\Controller;
use App\Models\Monitor\SsStatusPedido;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class FrtMonitorStatusPedidoController extends Controller
{
    public function index($empresa_id){

        $totais = SsStatusPedido::getTotais($empresa_id);

        $totais = json_encode($totais);

        $empresa= $empresa_id;


        return view('frontend.monitor_pedidos', compact('totais', 'empresa'));
    }

    public function Pedidos($empresa)
    {
        SsStatusPedido::notifica($empresa);
    }

    public function getPedidos($empresa_id)
    {

        $pedidos = SsStatusPedido::where('empresa',$empresa_id)->get();

        foreach ($pedidos as $pedido) {

            $tempo_maximo ='10';

            //Atribui classes
            $pedido->class_pedido = "success";
            $pedido->class_separacao = "danger";
            $pedido->class_conferencia = "danger";
            $pedido->class_entrega = "danger";


            //Validação da Classe
            if ($pedido->S0001){ // Pedido em separação
                $pedido->class_separacao = "info";
            }

            if($pedido->S0002 || $pedido->C0001){ // Separação do pedido concluida ou conferindo mercadorias
                $pedido->class_separacao = "success";
                $pedido->class_conferencia = "info";
            }

            if($pedido->C0002 || $pedido->E0001 || $pedido->E0002){ // Conferencia do pedido concluida ou entrega liberada ou entrega em transito
                $pedido->class_conferencia = "success";
                $pedido->class_entrega = "info";
            }

            if($pedido->E0003){
                $pedido->class_entrega = "success";
            }

            //Calcular diferença de tempos

            //Tempo separando mercadoria
            $pedido->t_iniciar_separa  = $this->calcDifHoras( $pedido->data_pedido,  $pedido->S0001); // Tempo para iniciar separação
            $pedido->t_limit_iniciar_separa = $this->verificaTempoLimite($pedido->data_pedido, '00:10:00'); // Verifica se esta no tempo estipulado

            $pedido->t_separando  = $this->calcDifHoras( $pedido->S0001,  $pedido->S0002); // Tempo total sparando
            $pedido->t_limit_separando = $this->verificaTempoLimite( $pedido->S0001, '00:10:00'); // Verifica se esta no tempo estipulado

            $pedido->t_total_separado  = $this->calcDifHoras( $pedido->data_pedido,  $pedido->S0002); // Tempo total até o fim da separação
            $pedido->t_limit_total_separado = $this->verificaTempoLimite( $pedido->data_pedido, '00:10:00'); // Verifica se esta no tempo estipulado




            //Conferencia
            $pedido->t_iniciar_conferencia  = $this->calcDifHoras(  $pedido->S0002,  $pedido->C0001); // Tempo para iniciar conferencia
            $pedido->t_limit_iniciar_conferencia = $this->verificaTempoLimite( $pedido->S0002, '00:10:00'); // Verifica se esta no tempo estipulado

            $pedido->t_conferindo  = $this->calcDifHoras( $pedido->C0001,  $pedido->C0002); // Tempo total conferindo
            $pedido->t_limit_conferindo = $this->verificaTempoLimite( $pedido->C0001, '00:10:00'); // Verifica se esta no tempo estipulado

            $pedido->t_total_conferido  = $this->calcDifHoras( $pedido->data_pedido,  $pedido->C0002); // Tempo total até o fim da conferencia
            $pedido->t_limit_total_conferido = $this->verificaTempoLimite( $pedido->data_pedido, '00:10:00'); // Verifica se esta no tempo estipulado

            //Entrega

            $pedido->t_iniciar_liberamotoboy  = $this->calcDifHoras(  $pedido->C0002,  $pedido->E0001); // Tempo para liberar o motoboy
            $pedido->t_limit_iniciar_liberamotoboy = $this->verificaTempoLimite( $pedido->C0002, '00:10:00'); // Verifica se esta no tempo estipulado


            // Verifica se existiu liberação para o motoboy
            if($pedido->E0001 == null && $pedido->E0002 <> null){ // Se o motoboy foi liberado e ja estra em transito, usa o status E0002 pq o E0001 fica null
               // $pedido->t_iniciar_liberamotoboy  = $this->calcDifHoras(  $pedido->C0002,  $pedido->E0002); // Tempo para liberar o motoboy
                $pedido->t_iniciar_transito  = $this->calcDifHoras(  $pedido->C0002,  $pedido->E0002); // Tempo para entrar em transito
                $pedido->t_limit_iniciar_transito = $this->verificaTempoLimite( $pedido->C0002, '00:10:00'); // Verifica se esta no tempo estipulado


            }else{
                //$pedido->t_iniciar_liberamotoboy  = $this->calcDifHoras(  $pedido->C0002,  $pedido->E0001); // Tempo para liberar o motoboy
                $pedido->t_iniciar_transito  = $this->calcDifHoras(  $pedido->E0001,  $pedido->E0002); // Tempo para entrar em transito
                $pedido->t_limit_iniciar_transito = $this->verificaTempoLimite( $pedido->E0001, '00:10:00'); // Verifica se esta no tempo estipulado

            }


            $pedido->t_iniciar_entregue  = $this->calcDifHoras(  $pedido->E0002,  $pedido->E0003); // Tempo para retorno do motoboy (nem sempre o E0002 esta preenchido. ---- verificar..
            $pedido->t_limit_iniciar_entregue = $this->verificaTempoLimite( $pedido->E0002, '00:10:00'); // Verifica se esta no tempo estipulado


            //tempo total do pedido
            if($pedido->modoentrega == '2'){ // se o modo entrega for motoboy
                 $pedido->tempo_total = $this->calcDifHoras(  $pedido->data_pedido,  $pedido->E0003); // Tempo para retorno do motoboy
            }else{ // se for balcao
                $pedido->tempo_total = $this->calcDifHoras(  $pedido->data_pedido,  $pedido->E0003); // Tempo para retorno do motoboy (de C0002 passou para E0003 apos notificacao do balcao

            }
            //Formatar hora
            $pedido->data_pedido =  Carbon::parse( $pedido->data_pedido)->format('d/m/Y H:i:s');

            if($pedido->S0002){
                $pedido->S0002 =  Carbon::parse( $pedido->S0002)->format('d/m/Y H:i:s');
            }
            if($pedido->C0002){
                $pedido->C0002 =  Carbon::parse( $pedido->C0002)->format('d/m/Y H:i:s');
            }
            if($pedido->E0003){
                $pedido->E0003 =  Carbon::parse( $pedido->E0003)->format('d/m/Y H:i:s');
            }

            //Descrição do status
            $pedido->descricao_status = $this->getDescricaoStatus($pedido->status);

            //Se for modo entrega balcão o status muda.

            if($pedido->status == 'C0002'  && $pedido->modoentrega == '3'){ //  Se for C0002 E modoentrega balcao..Era C0002 antes da notificação de balcao
                $pedido->descricao_status = 'Aguardando retirada no balcão';
            }
            if($pedido->status == 'E0003'  && $pedido->modoentrega == '3'){ //  se for E0003  E modoentrega balcao..Era C0002 antes da notificação de balcao
                $pedido->descricao_status = 'Entrega realizada no balcão';
            }

            //Informar o tipo de entrega
            if($pedido->modoentrega == '2'){
                $pedido->entrega = 'Motoboy';
            }else{
                $pedido->entrega = 'Balcão';
            }
        }
        return  response()->json($pedidos) ;
    }

    public  function getDescricaoStatus($status)
    {

        // Em caso de alteração neste bloco, verificar o FrtMonitorStatusController para manter identico
        switch ($status) {
            case 'A0001' :  $descricao_status = 'Aguardando separação'; break;
            case 'S0001' :  $descricao_status = 'Separando'; break;
            case 'S0002' :  $descricao_status = 'Aguardando conferência'; break;
            case 'C0001' :  $descricao_status = 'Em Conferência'; break;
            case 'C0002' :  $descricao_status = 'Aguardando liberar entrega'; break;
            case 'E0001' :  $descricao_status = 'Enrega liberada'; break;
            case 'E0002' :  $descricao_status = 'Saiu para entrega'; break;
            case 'E0003' :  $descricao_status = 'Entrega realizada'; break;
            case 'D0001' :  $descricao_status = 'Cancelado'; break;
        }

        return $descricao_status;
    }

    public function calcDifHoras($dateStart, $dateEnd)
    {

        if($dateStart <> null && $dateEnd <> null) {

            $dateStart = new \DateTime($dateStart);
            $dateEnd   = new \DateTime($dateEnd);
            $dateDiff = $dateStart->diff($dateEnd);

            if($dateDiff->d > 0){
                return $dateDiff->d. ' dia(s) '.$dateDiff->h .':'. str_pad($dateDiff->i, 2, '0', STR_PAD_LEFT).':'.str_pad($dateDiff->s, 2, '0', STR_PAD_LEFT);

            }else{
                return str_pad($dateDiff->h, 2, '0', STR_PAD_LEFT).':'. str_pad($dateDiff->i, 2, '0', STR_PAD_LEFT).':'.str_pad($dateDiff->s, 2, '0', STR_PAD_LEFT);
            }

        }else{
            return null;
        }

    }

    public function verificaTempoLimite($dataPedido, $tempo )
    {
        //Tempo maximo configurado no SSPlus
        $tempo = explode(':',$tempo);
        if($dataPedido <> null ) {

            $dataPedido = date_format( new \DateTime($dataPedido), 'Y-m-d H:i:s');

            $dateStart =  Carbon::createFromFormat('Y-m-d H:i:s',$dataPedido);
            $dateStart = $dateStart->addHours($tempo[0])->addMinutes($tempo[1])->addSeconds($tempo[2]);

           //Verirfica se passou do tempo limite de processo
            if($dateStart >= Carbon::now()){
               $t ='';
           }else {
               $t = 'tempoLimit';
           }

          /*  $dateStart = $dateStart + strtotime('00:10:00')*/;

            return $t;
        }
    }


}
