<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Empresa\Empresa;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
       
        return view('backend.dashboard');

    }

    public function trocarEmpresa($codigo)
    {

        $empresa = Empresa::find($codigo);

        //Auth::guard('web')->check();


        $usuario = User::find(auth()->user()->id);
        $usuario->empresa_id = $empresa->id;
        $usuario->empresa_codigo = $empresa->codigo;
        $usuario->save();


         return back();

    }
}
