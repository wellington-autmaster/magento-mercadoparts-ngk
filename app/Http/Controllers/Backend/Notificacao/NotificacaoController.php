<?php

namespace App\Http\Controllers\Backend\Notificacao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificacaoController extends Controller
{

    public function notificacoes(Request $request)
    {
        $notificacoes = $request->user()->notifications;

        return response()->json(compact('notificacoes'));
    }

}
