<?php

namespace App\Http\Controllers\Backend\MonitorSeparacao;


use App\Models\Monitor\Monitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;


// Senhas que ja foram ao monitor Mysql

class MonitorSeparacaoController extends Controller
{

    /**
     * @return Retorna para view os dados do monitor da empresa cadastrada
     */
    public function index()
    {
        $monitor  = Monitor::where('empresa',Auth()->user()->empresa_codigo)->first();
        return view('backend.MonitorSeparacao.index', compact('monitor'));
    }

    /**
     * @param Request $request : Recebe os dados do form com todas as configurações e persiste em banco de dados
     * @return mixed
     */
    public function update(Request $request)
    {
        // Seleciona o monitor a ser atualizado
        $monitor = Monitor::where('empresa', Auth()->user()->empresa_codigo)->first();

        $imgBackground = $request->file('img');
        $imgLogo =  $request->file('imgLogo');

        // Verifica se existe imagem de fundo
        if (File::exists($imgBackground)){
            $path = $imgBackground->storeAs('uploads', 'backupground_monitor.'.$imgBackground->extension(), 'public');
            $monitor->imgBackground = '/storage/'.$path; // Configura o nome da imagem para salvar no banco de dados.
        }
        // Verifica se existe imagem de logotipo
        if (File::exists($imgLogo)){
            $path = $imgLogo->storeAs('uploads', 'logo_empresa_monitor.'.$imgLogo->extension(), 'public');
            $monitor->imgLogo = '/storage/'.$path; // Configura o nome da imagem para salvar no banco de dados.
        }

        $monitor->headerColor = $request->headerColor;
        $monitor->headerTextColor = $request->headerTextColor;
        $monitor->senhas = $request->senhas;
        $monitor->modoentregabalcao = $request->modoentregabalcao;
        $monitor->proDescSize = $request->proDescSize;
        $monitor->proDescColor = $request->proDescColor;
        $monitor->proValSize = $request->proValSize;
        $monitor->proValColor = $request->proValColor;
        $monitor->noticias_fonte = $request->noticias_fonte;
        $monitor->qtdSenhas = $request->qtdsenhas;
        $monitor->noticias = $request->noticias;
        $monitor->conferencia = $request->conferencia;
        $monitor->qtdsenhasaguardando = $request->qtdsenhasaguardando;
        $monitor->save();

        //Atualiza cache
        Cache::forever('conferencia_'.$monitor->empresa, $monitor->conferencia);
        Cache::forever('modoentregabalcao_'.$monitor->empresa, $monitor->modoentregabalcao);
        return redirect()->route('admin.monitor.index')->withFlashSuccess('Configuração gravadas com sucesso! :)');
    }








}
