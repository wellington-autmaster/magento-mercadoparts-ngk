<?php

namespace App\Http\Controllers\Backend\Marktplace\Produto;

use App\Http\Controllers\Controller;
use App\Models\Marktplace\ProdutoVeiculo;
use App\Models\Marktplace\Veiculo;
use App\Models\Marktplace\VeiculoMarca;
use App\Models\Marktplace\VeiculoTipo;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;

class VeiculosController extends Controller
{

    public function getMarcas($tipo)
    {
        return response(VeiculoTipo::find($tipo)->marcas,200);
    }

    public function getVeiculos($marca)
    {
        return response(VeiculoMarca::find($marca)->veiculos,200);
    }

    public function getAnos($veiculo,$produto)
    {
        /*Busca anos vinculados aos veiculos
        * Primeiro, busca os anos que ja estão vinculados a cada veiculo na tabela ProdutoVeiculo
         * Depois cria um array com todos os ids que ja estao vinculados ao produto e o veiculo
         * Puxa os anos, meno os ja registrados, com o where not in */

        // Pesquisa anos ja vinculados ao produto
        $anosVinculados = ProdutoVeiculo::select('ano_id')
            ->where('veiculo_id',$veiculo)->where('ssproduto_id',$produto)->get();

        // Cria um array com os anos para usar posteriormente
        $arrAnosVinculados = [];
        foreach ($anosVinculados as $ano)
        {
            $arrAnosVinculados[]+= $ano->ano_id;
        }

        //Busca os anos vinculados ao veiculo, tirando os anos ja vinculados no produto
        $anos= Veiculo::find($veiculo)->anos
            ->whereNotIn('id',$arrAnosVinculados);

        return response($anos,200);
    }
}
