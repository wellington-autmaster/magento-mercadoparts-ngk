<?php

namespace App\Helpers\Backend\Marktplace;

use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\ProdutoVeiculo;
use App\Models\Marktplace\ProdutoCaracteristica;
use Illuminate\Support\Facades\Log;
use App\Models\Marktplace\SSProdutosEcommerce;
use GuzzleHttp\Client;


class HelperPecaaqui 
{

    public function enviaProdutosPecaAqui($i,$produto)
    {
        //Tipo de atualização
       $empresas = Marktplace::where('status',true)->where('codigo','pecaqui')->get();

       foreach($empresas as $empresa){
        $i=0;
           $up = Marktplace::TipoAtualizacao($i,$produto, $empresa->empresa,'atualizacao_geral_pa');
           //seleciona os produtos para enviar ao ecommerce
            $produtos = SSProdutosEcommerce::where('codigo_empresa', $empresa->empresa)
            ->where($up['coluna'],$up['operador'],$up['valor'])->orderBy('codigo_interno','desc')->chunk(100, function ($produtos) use($empresa,$i){
            //Captura o token do peca aqui
            
        
                foreach ($produtos as $produto) {
                    $pecaqui= Marktplace::pecaquiToken($empresa->empresa);
                    Log::info('Peçaaqui - Sincronização '.$produto->codigo_interno .' - '. $produto->descricao);

                    //Imagem -- Captura os dados binarios do postgres e converte em string 
                    if(isset($produto->foto_1)){
                        $link = ENV('APP_URL').'imagem/produto/' .$empresa->empresa . '/foto_1/' . $produto->codigo_interno  ;
                    }else{
                        $link ='';
                    };

                    //Captura 10 Caracteristicas para enviar
                    $caracateristicas = ProdutoCaracteristica::produtoCarateristica($produto->codigo_interno);

                    $i=0;
                    foreach($caracateristicas as $c){
                        $i++;
                        $form_params['caracteristica'.$i] = $c['descricao'] .' - '.$c['valor'];   
                    }

                    // Prepara a headers com autenticação
                    $headers = [
                        'api_token' => $pecaqui->api_token,
                        'vendedor_key' => $pecaqui->vendedor_key,
                    ];

                    //Ajusta quebra de linha da aplicação
                    $produto->aplicacao = str_replace("\r", "<br>", $produto->aplicacao);

                    //Primeiro envia o produto de forma "Universal" depois envia de acordo com o veiculo
                    $form_params['codigo']  = $produto->codigo_interno;
                    $form_params['nome']    = $produto->descricao;
                    $form_params['ano']     =  0000;  
                    $form_params['marca']   = 'Geral';
                    $form_params['modelo']  = 'Geral';
                    $form_params['submodelo'] = 'Geral';
                    $form_params['categoria'] = $produto->grupo_descricao;
                    $form_params['preco']   = $produto->preco_venda;
                    $form_params['quantidade'] = $produto->estoque_disponivel;
                    $form_params['descricao'] = $produto->aplicacao;
                    $form_params['avatar']  = $link;//"data:image/png;base64,".$html_data."",
                    $array = ['form_params' => $form_params, 'headers' => $headers];

                    $client = new Client(); 
                    $response = $client->request('POST', 'http://pecaaqui.com/rest/product',$array);

                    //Seleciona os veiculos para envio
                    $veiculos = ProdutoVeiculo::where('ssproduto_id', $produto->codigo_interno)->get();

                    foreach ($veiculos as $veiculo) {

                                            //Ajusta parametros da API
                        $form_params['ano']     = $veiculo->ano->ano_modelo;  
                        $form_params['marca']   = $veiculo->ano->marca;
                        $form_params['modelo']  = $veiculo->ano->name;
                        $form_params['submodelo'] = $veiculo->ano->combustivel;

                        $array = ['form_params' => $form_params, 'headers' => $headers];
                        //Envia para API
                        $client = new Client(); 
                        $response = $client->request('POST', 'http://pecaaqui.com/rest/product',$array);
                        
                    }
                    
                } //foreach
            }); // Select de 100 em 100
            $i=0;
            Log::info($i++ . 'Sincronização com Peçaaqui finalizada com sucesso!');
            return null;
       }
        
          
    }//function

}