<?php
/**
 * Created by PhpStorm.
 * User: wanderlei
 * Date: 20/09/18
 * Time: 10:32
 */

namespace App\Helpers\Backend\Marktplace;

use App\Jobs\FilaProdutosMagento23;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use App\Models\Auth\User;
use App\Models\Empresa\Configuracao;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\SSProdutosEcommerce;
use App\Notifications\Backend\Marktplace\UpdateProdutos;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class HelperMagento23
{
    public $homologacao = '';

    public function __construct()
    {
        if (config('magento.homologacao')) {
            $this->homologacao = '/homolog/';
        }
    }

    public function ProdutosFila()
    {
        Log::alert('Iniciando atualização de produtos do Magento');

        //Verifica os marktplaces de cada empresa
        $empresas = Marktplace::Empresas('magento23');

        foreach ($empresas as $empresa) {

            SSProdutosEcommerce::ProdutosParaAtualizar($empresa->empresa, 'magento23')->chunk(300, function ($produtos) use ($empresa) {
                foreach ($produtos as $produto) {
                    FilaProdutosMagento23::dispatch($empresa, $produto->codigo)->onQueue('ecommercce');
                }
            });
            FilaProdutosMagento23::dispatch($empresa, '9999999')->onQueue('ecommercce');
        }
        return true;
    }


    public function enviaProdutosMagento23($empresa, $produto)
    {

        if($produto =='9999999'){
            $users = User::all();
            Notification::send( $users, new UpdateProdutos('Todos os preços e estoque do e-commerce(Magento) foram atualizados para empresa '. $empresa->empresa));
            return true;
        }


        $produto = SSProdutosEcommerce::where('codigo_interno', $produto)->select(['codigo_interno', 'estoque_disponivel', 'preco_venda', 'preco_venda_atacado', 'data_inicio_promocao', 'data_fim_promocao', 'preco_promocao'])->where('codigo_empresa',$empresa->empresa)->first();
        if (!$produto){
            exit();
        }

        $TodosProdutosMagento = $this->getTodosProdutosMagento($produto->codigo_interno);
        if (!$TodosProdutosMagento) {
            Log::alert('Produto não está cadastrado no Magento. Código SSPlus:' . $produto->codigo_interno);
            exit();
        }else{
            foreach ($TodosProdutosMagento as $produtoMagento) {

                if ($produtoMagento->custom_attributes['4']->value == $produto->codigo_interno) {
                    $sku = $produtoMagento->sku;
                    $json_produto = $this->formaJsonProduto($produto, $sku);

                    try {

                       $this->updateProdutoMagento($json_produto, $sku);

                    } catch (ClientException $e) {
                        Log::emergency($empresa->empresa. 'enviaPrecoEstoque: '. 'Ocorreram problemas com atualização do produto '. $produto->codigo_interno.' - ' . $e->getMessage()  ,[$produto]);
                    };

                }
            }
        }

    }



    public function getTodosProdutosMagento($sku){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();


            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
            ]);

        try{

            $uri = "$this->homologacao./rest/V1/products/?searchCriteria[filter_groups][0][filters][0][field]=sku_integration&searchCriteria[filter_groups][0][filters][0][value]=". $sku;
            $http = $cliente->get($uri, ['auth' => 'oauth']);


            if ($http->getStatusCode() != '200') {
                exit($http->getStatusCode() . 'Internal Server Error. '.   'Rota: ' . $uri);
            }

            $res = $http->getBody()->getContents();
            $produtos_magento = json_decode($res);
            $retorno = $produtos_magento->items;
            return $retorno;



        } catch (ClientException $e) {
            Log::emergency( 'Linha 269 HelperMagento: '. 'Magento Produto - Ocorreram problemas com verificar produto '.  $e->getMessage() );
        };

    }


    public function getProdutoMagento($empresa, $sku){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        try{
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
            ]);

            $uri = "$this->homologacao./rest/V1/products/". $sku;
            $getProduto = $cliente->get($uri, ['auth' => 'oauth']);

            Log::debug($getProduto->getStatusCode());


            if($getProduto->getStatusCode() == '404') { //Produto não existe no magento
                return false;
            }

            if($getProduto->getStatusCode() == '200') { // Produto existe no magento
                return $getProduto->getBody()->getContents();
            }

            if($getProduto->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($getProduto->getStatusCode() . ' -  Erro interno Magento');
            }


        } catch (ClientException $e) {
            Log::emergency( 'Linha 269 HelperMagento: '. 'Magento Produto - Ocorreram problemas com verificar produto '.  $e->getMessage() );
        };

    }



    public function formaJsonProduto($produto, $sku){

        //Promocao
        /*
        $arrprodutos_custom = [];
        $inicio_promocao =
            [
                'attribute_code'       =>   'special_from_date',
                'value'          =>  $produto->data_inicio_promocao,
            ];
        $fim_promocao =
            [
                'attribute_code'       =>   'special_to_date',
                'value'          =>  $produto->data_fim_promocao,
            ];
        $preco_promocao =
            [
                'attribute_code'       =>   'special_price',
                'value'          =>   $produto->preco_promocao,
            ];
        //pega os dados sobre as promoções
        array_push($arrprodutos_custom, $preco_promocao, $inicio_promocao, $fim_promocao);
        */

        //define se está habilitado ou não
        $produto->excluir_site  ? $status = 2 : $status = 1;
        //verifica status estoque
        $produto->estoque_disponivel <= '0' ? $statusEstoque = false : $statusEstoque= true;
        $estoque = $produto->estoque_disponivel;
        //atributo do produto
        $extensionAttributes =[
            'stock_item' => [
                'qty' =>  $estoque,
                'is_in_stock' =>  $statusEstoque
            ]
        ];

        $arrprodutos = [
            'sku' => $sku,
            'price'  => $produto->preco_venda_atacado,
            'extension_attributes' => $extensionAttributes
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_produtos = [
            'product' => $arrprodutos
        ];

        // transforma em json para o envio
        $produtos = json_encode($arr_produtos);
        return $produtos;

    }


    public function getCategoriaMagento($empresa, $attribute_ssplus){

        try {

            if (Cache::has($attribute_ssplus)) {
                return Cache::get($attribute_ssplus);
            }

                $url_api = $this->getUrlApi();
                $Oauth1Magento = $this->getConfigMagento();

                $headers = [
                    'Content-Type' => 'application/json',
                    'Accept' => '*/*'
                ];

                $stack = HandlerStack::create();
                $oauth = new Oauth1($Oauth1Magento);
                $stack->push($oauth);

                $cliente = new Client([
                    'base_uri' => $url_api,
                    'headers' => $headers,
                    'handler' => $stack,
                ]);

                Log::debug('teste homolog'. $this->homologacao);

                // Set the "auth" request option to "oauth" to sign using oauth
                $rotaPesquisaCategoria = $this->homologacao."/rest/V1/categories/list?searchCriteria[filterGroups][0][filters][0][field]=attribute_ssplus&searchCriteria[filterGroups][0][filters][0][value]=" . $attribute_ssplus;
                $http = $cliente->get($rotaPesquisaCategoria, ['auth' => 'oauth']);
                if ($http->getStatusCode() != '200') { // Timeout interrompe o script
                    Log::debug('Status Code, pesquisa categoria: '. $http->getStatusCode());
                    exit($http->getStatusCode() . ' -  Erro interno ao consultar MegaCategoria');
                }

                $verificaCategoriasExistentes = $http->getBody()->getContents();
                $verificaCategoriasExistentes = json_decode($verificaCategoriasExistentes);
                Log::debug('bodyCategorias'. $http->getBody());
                Log::debug('headersCategorias'. json_encode($http->getHeaders()));

                if (!$verificaCategoriasExistentes->total_count) {
                    return false;
                }

                $id_categoria = $verificaCategoriasExistentes->items[0]->id;

                Cache::put($attribute_ssplus, $id_categoria , 60);

                return $id_categoria;



        } catch (ClientException $e) {
            Log::emergency('HelperMagento: ' . 'Magento Categorias - Ocorreram problemas com verificação da categoria ' . $e->getMessage());
        }
    }



    public function VerificaMegaCategoria($empresa, $produto, $attribute_ssplus)
    {
        $categoria_id = $this->getCategoriaMagento($empresa, $attribute_ssplus);


            if($categoria_id){
                $res_megacategoria = $this->atualizaMegaCategoria($empresa, $produto, $categoria_id, $attribute_ssplus);
            }else{
                $res_megacategoria = $this->adicionaMegaCategoria($empresa, $produto, $attribute_ssplus);
            }
            return $res_megacategoria;
    }

    public function verificaCategoria($empresa, $produto, $id_megacategoria, $attribute_ssplus)
    {
        $categoria_id = $this->getCategoriaMagento($empresa, $attribute_ssplus);

        if($categoria_id){
             return $this->atualizaCategoria($empresa, $produto, $categoria_id, $id_megacategoria, $attribute_ssplus);
        }else{
             return $this->adicionaCategoria($empresa, $produto, $id_megacategoria, $attribute_ssplus);
        }
    }


    public function adicionaSubCategoria($empresa, $produto, $id_secao, $attribute_ssplus){
        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();


        $arrsubcategoria = [
            'name'   => $produto->grupo_descricao,
            'include_in_menu' => true,
            'level' => '4',
            'parent_id'  => $id_secao,
            'isActive' => true,
            'custom_attributes' => [
                [
                    'attribute_code' => 'attribute_ssplus',
                    'value' => $attribute_ssplus,
                ],
                [
                    'attribute_code' => 'url_key',
                    'value' => Str::slug('grupo-'. $produto->grupo_descricao, '-')
                ]
            ],
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_subcategoria = [
            'category' => $arrsubcategoria,
            'saveOptions' => true
        ];

        // transforma em json para o envio
        $categoria = json_encode($arr_subcategoria);

        try {
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];
            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);


            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $cliente->post($this->homologacao."/rest/V1/categories",['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao criar Sub-Categoria Magento');
            }

            $res_subcategoria = $http->getBody()->getContents();
            $res_subcategoria = json_decode($res_subcategoria);
            return $res_subcategoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Sub-Categoria - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }

    public function adicionaCategoria($empresa, $produto, $id_megacategoria, $attribute_ssplus){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();



        $arrcategoria = [
            'name'   => $produto->secao_descricao,
            'parent_id'  => $id_megacategoria,
            'level' => '3',
            'include_in_menu' => true,
            'isActive' => true,
            'custom_attributes' => [
                [
                    'attribute_code' => 'attribute_ssplus',
                    'value' => $attribute_ssplus,
                ],
                [
                    'attribute_code' => 'url_key',
                    'value' => Str::slug( 'secao-'. $produto->secao_descricao, '-')
                ]
            ],
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_categoria = [
            'category' => $arrcategoria,
            'saveOptions' => true
        ];
        // transforma em json para o envio

        $categoria = json_encode($arr_categoria);
        if(config('app.env') === 'local' ) {
            Log::debug('categoria json:' . $categoria);
        }

        try {

            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);

            $http = $cliente->post($this->homologacao."/rest/V1/categories",['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao criar Categoria Magento');
            }

            $res_categoria = $http->getBody()->getContents();
            $res_categoria = json_decode($res_categoria);
            return $res_categoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Categoria Principal - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }



    public function adicionaMegaCategoria($empresa, $produto, $attribute_ssplus){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();


        $arrcategoria = [
            'name'   => $produto->subgrupo_descricao,
            'parent_id'  => '2',
            'level' => '2',
            'include_in_menu' => true,
            'isActive' => true,
            'custom_attributes' => [
               [
                   'attribute_code' => 'attribute_ssplus',
                   'value' => $attribute_ssplus,
               ],
               [
                   'attribute_code' => 'url_key',
                   'value' => Str::slug($produto->subgrupo_descricao, '-')
               ]
            ],
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_categoria = [
            'category' => $arrcategoria,
            'saveOptions' => true
        ];
        // transforma em json para o envio

        $categoria = json_encode($arr_categoria);
        if(config('app.env') === 'local' ) {
            Log::debug('categoria json:' . $categoria);
        }

        try {

            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);

            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $cliente->post($this->homologacao."/rest/V1/categories", ['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao criar Mega Categoria Magento');
            }

            $res_megacategoria = $http->getBody()->getContents();
            $res_megacategoria = json_decode($res_megacategoria);
            return $res_megacategoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Categoria Principal - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }

    public function atualizaMegaCategoria($empresa, $produto, $idCategoria, $attribute_ssplus){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        $arrcategoria = [
            'id' => $idCategoria,
            'name'   => $produto->subgrupo_descricao,
            'parent_id'  => '2',
            'level' => '2',
            'include_in_menu' => true,
            'isActive' => true,
            'custom_attributes' => [
                [
                    'attribute_code' => 'attribute_ssplus',
                    'value' => $attribute_ssplus,
                ],
                [
                    'attribute_code' => 'url_key',
                    'value' => Str::slug($produto->subgrupo_descricao, '-')
                ]
            ],
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_categoria = [
            'category' => $arrcategoria,
            'saveOptions' => true
        ];
        // transforma em json para o envio
        $categoria = json_encode($arr_categoria);
        if(config('app.env') === 'local' ) {
            Log::debug('categoria json:' . $categoria);
        }

        try {

            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);



            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $cliente->put($this->homologacao."/rest/V1/categories/". $idCategoria,['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao atualizar Mega Categoria Magento');
            }


            $res_megacategoria = $http->getBody()->getContents();
            $res_megacategoria = json_decode($res_megacategoria);
            if(config('app.env') === 'local' ) {
                Log::debug('RESPOSTA MEGA CATEGORIA ATUALIZADA: ' . $res_megacategoria->id);
            }
            return $res_megacategoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Categoria Principal - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }


    public function atualizaSubCategoria($empresa, $produto, $idCategoria, $id_secao,  $attribute_ssplus){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        $arrcategoria = [
            'id' => $idCategoria,
            'name'   => $produto->grupo_descricao,
            'parent_id'  => $id_secao,
            'level' => '4',
            'include_in_menu' => true,
            'isActive' => true,
            'custom_attributes' => [
                [
                    'attribute_code' => 'attribute_ssplus',
                    'value' => $attribute_ssplus,
                ],
                [
                    'attribute_code' => 'url_key',
                    'value' => Str::slug('grupo-'. $produto->grupo_descricao, '-')
                ]
            ],
        ];
        //adiciona os parâmetros necessário para o envio
        $arr_categoria = [
            'category' => $arrcategoria,
            'saveOptions' => true
        ];
        // transforma em json para o envio
        $categoria = json_encode($arr_categoria);
        if(config('app.env') === 'local' ) {
            Log::debug('categoria json:' . $categoria);
        }

        try {
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);

            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $cliente->put($this->homologacao."/rest/V1/categories/". $idCategoria,['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao atualizar Sub-Categoria Magento');
            }
            $res_megacategoria = $http->getBody()->getContents();
            $res_megacategoria = json_decode($res_megacategoria);
            return $res_megacategoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Categoria Principal - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }


    public function atualizaCategoria($empresa, $produto, $idCategoria, $id_megacategoria,  $attribute_ssplus){

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        $arrcategoria = [
            'id' => $idCategoria,
            'name'   => $produto->secao_descricao,
            'parent_id'  => $id_megacategoria,
            'level' => '3',
            'include_in_menu' => true,
            'isActive' => true,
            'custom_attributes' => [
                [
                    'attribute_code' => 'attribute_ssplus',
                    'value' => $attribute_ssplus,
                ],
                [
                    'attribute_code' => 'url_key',
                    'value' => Str::slug( 'secao-'. $produto->secao_descricao, '-')
                ]
            ],
        ];

        //adiciona os parâmetros necessário para o envio
        $arr_categoria = [
            'category' => $arrcategoria,
            'saveOptions' => true
        ];
        // transforma em json para o envio
        $categoria = json_encode($arr_categoria);
        if(config('app.env') === 'local' ) {
            Log::debug('categoria json:' . $categoria);
        }

        try {

            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ];

            $stack = HandlerStack::create();
            $oauth = new Oauth1($Oauth1Magento);
            $stack->push($oauth);

            $cliente = new Client([
                'base_uri' => $url_api,
                'headers' => $headers,
                'handler' => $stack,
                'body' => $categoria
            ]);

            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $cliente->put($this->homologacao."/rest/V1/categories/". $idCategoria,['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                exit($http->getStatusCode() . ' -  Erro interno ao atualizar Categoria Magento');
            }

            $res_categoria = $http->getBody()->getContents();
            $res_categoria = json_decode($res_categoria);
            return $res_categoria->id;

        } catch (ClientException $e) {
            Log::emergency( 'HelperMagento: '. 'Magento Categoria Principal - Ocorreram problemas com a criação da categória '.  $e->getMessage() );
        };

    }

    public function verificaSubCategoria($empresa, $produto, $id_secao, $attribute_ssplus)
    {
        $categoria_id = $this->getCategoriaMagento($empresa, $attribute_ssplus);


        if($categoria_id){
            return $this->atualizaSubCategoria($empresa, $produto, $categoria_id, $id_secao, $attribute_ssplus);
        }else{
            return $this->adicionaSubCategoria($empresa, $produto, $id_secao, $attribute_ssplus);
        }
    }


    public function AddProduto($empresa, $produtos, $headers)
    {

        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        $stack = HandlerStack::create();
        $oauth = new Oauth1($Oauth1Magento);
        $stack->push($oauth);

        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
            'body' => $produtos,
        ]);

        // Set the "auth" request option to "oauth" to sign using oauth
        $res = $client->post("rest/V1/products",['auth' => 'oauth']);
        return $res;
    }


    public function updatePrecoProduto($produtos, $headers)
    {
        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();


        $stack = HandlerStack::create();
        $oauth = new Oauth1($Oauth1Magento);
        $stack->push($oauth);

        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
            'body' => $produtos,
        ]);

        // Set the "auth" request option to "oauth" to sign using oauth

        $res = $client->post("rest/V1/products", ['auth' => 'oauth']);

        return $res;
    }

    public function updateProdutoEstoque($produtos, $headers)
    {
        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();


        $stack = HandlerStack::create();
        $oauth = new Oauth1($Oauth1Magento);
        $stack->push($oauth);

        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
            'body' => $produtos,
        ]);

        // Set the "auth" request option to "oauth" to sign using oauth

        $res = $client->put("rest/V1/sellersUpdateStockErp", ['auth' => 'oauth']);

        if($res->getStatusCode() == '404') { //Produto não existe no magento
            return false;
        }

        if($res->getStatusCode() == '200') { // Atualizado com sucesso
            return $res->getBody()->getContents();
        }

        if($res->getStatusCode() != '200' ) { // Timeout interrompe o script
            exit($res->getStatusCode() . ' -  Erro interno Magento');
        }
    }

    public function updateProdutoMagento($json_produto, $sku)
    {
        $url_api = $this->getUrlApi();
        $Oauth1Magento = $this->getConfigMagento();

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        ];

        $stack = HandlerStack::create();
        $oauth = new Oauth1($Oauth1Magento);
        $stack->push($oauth);

        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
            'body' => $json_produto,
        ]);

        // Set the "auth" request option to "oauth" to sign using oauth

        $res = $client->put("rest/V1/products/". $sku, ['auth' => 'oauth']);


        if($res->getStatusCode() == '404') { //Produto não existe no magento
            return false;
        }

        if($res->getStatusCode() == '200') { // Atualizado com sucesso
            Log::notice('Produto Cód. Magento: '. $sku. ' Atualizado com sucesso!');
            return $res;
        }

        if($res->getStatusCode() != '200' ) { // Timeout interrompe o script
            exit($res->getStatusCode() . ' -  Erro interno Magento');
        }

    }



    public function getCaracteristicas($produto,  $arrprodutos_custom){

        $caracteristicas = DB::connection('pgsql')->table('produtos_ecommerce_caracteristicas')->where('codigo_interno', $produto->codigo_interno)->get();



        foreach ($caracteristicas as $caracteristica){
            array_push($arrprodutos_custom, [
               'attribute_code' => trim(strtolower($caracteristica->descricao_caracteristica)),
               'value' => trim(strtolower($caracteristica->valor_caracteristica))
            ]);
        }

        return $arrprodutos_custom;

    }

    public function getImagemDoProduto($produto){

        $imagens = DB::connection('pgsql')->table('imagem_produto')->where('codigo', $produto->codigo_interno)->get();

        $arr_imagens = [];

        foreach ($imagens as $imagem){
            //Ajusta codificação da imagem
            $my_bytea = stream_get_contents($imagem->arquivo);
            $my_string = pg_unescape_bytea($my_bytea);
            $html_data = htmlspecialchars($my_string);


         array_push($arr_imagens, [
             "position" => $imagem->ordem,
             "media_type" => "image",
             "disabled" => "false",
             "types" => [
               "image",
               "small_image",
               "thumbnail"
             ],
             "content" => [
                'base64_encoded_data' => $html_data,
                'type' => "image/jpeg",
                'name' => $produto->codigo_interno. '-' . $imagem->ordem. '.jpg',
            ]]);
        }


        $ImagemDoProduto = $arr_imagens;

        return $ImagemDoProduto;


    }




    public function validaProduto($produto)
    {
        if (trim($produto->codigo_fabricante) == null) {
            Log::info('Produto não possui Código do Fabricante no cadastro do SSPlus',
                ['cod: ' => $produto->codigo_interno]);

            return false;
        };

        if (trim($produto->codigo_pesquisa1) == null) {
            Log::info('Produto não possui Código de Pesquisa(1) no cadastro do SSPlus',
                ['cod: ' => $produto->codigo_interno]);

            $produto->codigo_pesquisa1=null;
        };


        if (trim($produto->codigo_barras) == null) {
            Log::info('Produto não possui código de barras no cadastro do SSPlus',
                ['cod: ' => $produto->codigo_interno]);
        }

        if (trim($produto->preco_venda) == '0.00') {
            Log::info('Produto sem preço de venda definido no cadastro',
                ['cod: ' => $produto->codigo_interno]);

            return false;
        }

        if (trim($produto->peso_bruto) == null) {
            Log::info('O peso do produto não esta definido no cadastro, valor 1 é definido como padrão.',
                ['cod: ' => $produto->codigo_interno]);

            $produto->peso_bruto = 1;
        }

        if (trim($produto->comprimento) == null) {
            Log::info('O comprimento do produto não esta definido no cadastro, valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);
            $produto->comprimento = 1;
        }

        if (trim($produto->largura) == null) {
            Log::info('A largura do produto não esta definido no cadastro, valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);
            $produto->largura = 1;
        }

        if (trim($produto->altura) == null) {
            Log::info('A altura do produto não esta definido no cadastro,  valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);

            $produto->altura = 1;
        }
        if (trim($produto->descricao) == null) {
            Log::info('Produto não possui descrição no cadastro',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->aplicacao) == null) {
            Log::info('Produto não possui aplicação no cadastro ',
                ['cod: ' => $produto->codigo_interno]); 
        }else{
            $aplicacao = strtoupper($produto->aplicacao);
            $aplicacao_sem_similares = explode('SIMILAR/CORRELACIONADOS', $aplicacao);

            if (isset($aplicacao_sem_similares[0])){
                $produto->aplicacao = $aplicacao_sem_similares[0];
            }
        }

        if (trim($produto->secao) == null) {
            Log::info('Produto  não possui secao',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->grupo) == null) {
            Log::info('Produto  não possui grupo',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->grupo_descricao) == null) {
            Log::info('Produto não possui descricao no grupo',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->secao_descricao) == null) {
            Log::info('Produto  não possui descricao na secao',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->marca) == null) {
            Log::info('Produto não possui Marca definida, por padrão será relacionado a marca "Geral"',
                ['Cod:' => $produto->codigo_interno]);
            $produto->marca = "GERAL";
        }

        //Valida se esta ativo ou não
        $produto->status = $produto->excluir_site == false ? true : false;

        //Se estiver marcado para verificar foto, verifica se existe fotos no produto, se não existir o produto é inativado no e-commerce
        if(env('APP_ECOMM_VERIFICA_FOTO')){
            if($produto->foto_1){
                $produto->status = false;
           }
        }

        //Faz a troca do true/false para os números(deverá ser informado para o magento o cod 1=true ou 2=false)
        $produto->excluir_site ==false ? $produto->status=2 : $produto->status=1;


        // Enriquece aplicação
       // $aplica =  "<strong>Descrição: </strong> $produto->descricao <br>" .
         //   "<strong>Marca: </strong> $produto->marca <br>" .
         //   "<strong>Código Fabricante: </strong>  $produto->codigo_fabricante<br>" .
        //    "<strong>Código de barras: </strong>  $produto->codigo_barras<br><hr><br> $produto->aplicacao  "   ;
     //   $produto->aplicacao = $aplica;


        /** No aba e-commerce do SSPlus é possivel personalizar uma seção, caso ela esteja preenchida no cadastro de
         * produtos, irá substituir a da aba principal.
         * secao = recebe codigo 8 na frente
         * codigo_secao_ecommerce = Recebe codigo 9 na frente
         */
        if (trim($produto->descricao_secao_ecommerce) != trim($produto->secao_descricao) &&
            trim($produto->codigo_secao_ecommerce) != null &&
            trim($produto->descricao_secao_ecommerce) != null
        ) { //não deixa gravar outra seção se a descrição for igua a principal
            $produto->secao_descricao = trim($produto->descricao_secao_ecommerce);
            $produto->secao = (integer)'9' . $produto->codigo_secao_ecommerce;

        } else {
            $produto->secao = (integer)'8' . $produto->secao;
        }

        $produto->product_id = (integer)$produto->product_id;

        return $produto;
    }



    public function validaConfiguracoes($empresa, $produto)
    {
        //Configurações -- Valida promoção
        $config = Configuracao::where('modulo', 'magento23')->where('configuracao', 'enviar_promocao')->where('empresa', $empresa)->first();
        if ($config->valor == false) {
            $produto->preco_promocao = null;
            $produto->data_inicio_promocao = null;
            $produto->data_fim_promocao = null;
        };



        // Enriquece aplicação
        $aplica =  "<strong>Descrição: </strong> $produto->descricao <br>" .
            "<strong>Marca: </strong> $produto->marca <br>" .
            "<strong>Código interno: </strong>  $produto->codigo_interno<br>" .
            "<strong>Código Fabricante: </strong>  $produto->codigo_fabricante<br>" .
            "<strong>Código Original: </strong>  $produto->codigo_pesquisa1<br>" .
            "<strong>Código de barras: </strong>  $produto->codigo_barras<br><hr><br> $produto->aplicacao  "   ;
        $produto->aplicacao = $aplica;


        //marca_na_descricao
        $config = Configuracao::where('modulo','magento23')->where('configuracao','marca_na_descricao')->where('empresa',$empresa)->first();
        if($config->valor == true){
            $produto->descricao = $produto->descricao.' '. $produto->marca;
        }

        //atualizar_descricao
        $config = Configuracao::where('modulo','magento23')->where('configuracao','atualizar_descricao')->where('empresa',$empresa)->first();
        $produto->description_update = $config->valor;

        //inativa_produto_sem_estoque
        $config = Configuracao::where('modulo','magento23')->where('configuracao','inativa_produto_sem_estoque')->where('empresa',$empresa)->first();
        if($config->valor == true){
            if($produto->estoque_disponivel <= 0){
                $produto->status = 0;
            }
        }
        /*        Inativar produtos de classes não permitidas*/

        //Verificar classes configuradas para não enviar ao e-commerce
   /*     $classe_nao_enviar = Configuracao::where('modulo','magento23')->where('configuracao','classe_nao_enviar')->where('empresa',$empresa)->first();
        $classesConfiguradas = explode(';', $classe_nao_enviar->valor); //O formato salvo ex: A,B,C...

        // Verifica se o produto esta atribuido a uma classe
        if( $produto->classe_abc == '' ) {
            $produto->classe_abc = [];
        }
        //Verifica se a classe do produto esta contido nas classes para não enviar ao e-commerce
        $classeAnalisada = array_diff([$produto->classe_abc], $classesConfiguradas);

        if($classeAnalisada !== [$produto->classe_abc]) {
            //Caso a classse esteja incluida na lista de bloqueio, desabilita o produto no e-commerce
            $produto->status = 0;
        }
*/
        return $produto;

    }



    public function getUrlApi() //Marktplace $marktplace)
    {
         $url_api = config('magento.url_magento');
        return $url_api;
   //    return
    }

    public function getSellerId()
    {
        $seller_id = config('magento.seller_id');

        return $seller_id;
    }

    public function getConfigMagento() //Marktplace $marktplace
    {

    /*    $Oauth1Magento = [
        'consumer_key'    => $marktplace->usuario,
        'consumer_secret' => $marktplace->senha,
        'token'           => $marktplace->client_id,
        'token_secret'    => $marktplace->client_secret
    ]; */
        $Oauth1Magento = config('magento.Oauth1');

        return $Oauth1Magento;
    }




}
