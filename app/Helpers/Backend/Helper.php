<?php

if (!function_exists('link_img_produto_principal')) {

    /**
     * @param $codigo_interno (Código do produto SSPlus)
     * @return string com link da imagem do produto
     */
    function link_img_produto_principal($codigo_interno)
    {
        $link = ENV('APP_URL') . '/imagem/produto/0/' . $codigo_interno;
        return $link;
    }
}


if (!function_exists('hex2RGB')) {
    function hex2RGB($hex)
    {
        $hex = str_replace("#", "", $hex);
        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = 'RGB('.$r.','.$g.','.$b.')';
        return $rgb;
    }
}