<?php

namespace App\Providers;

use App\Events\Backend\Opencart\EventAtualizaProdutoOpencart;
use App\Events\Backend\SSPlus\EventNovoPedido;
use App\Listeners\Backend\MonitorPedidos\CadastraPedidosSeparacao;
use App\Listeners\Backend\Opencart\EnviarAtributos;
use App\Listeners\Backend\Opencart\EnviarImagem;
use App\Listeners\Backend\Opencart\EnviarProduto;
use App\Listeners\Backend\Opencart\EnviarSimilates;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        EventAtualizaProdutoOpencart::class => [
            EnviarProduto::class,
            EnviarImagem::class,
            EnviarAtributos::class,
            EnviarSimilates::class,
        ],
        EventNovoPedido::class => [
            CadastraPedidosSeparacao::class,
        ]
    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        /*
         * Frontend Subscribers
         */

        /*
         * Auth Subscribers
         */
        \App\Listeners\Frontend\Auth\UserEventListener::class,

        /*
         * Backend Subscribers
         */

        /*
         * Auth Subscribers
         */
        \App\Listeners\Backend\Auth\User\UserEventListener::class,
        \App\Listeners\Backend\Auth\Role\RoleEventListener::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
