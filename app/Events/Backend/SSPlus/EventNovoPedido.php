<?php

namespace App\Events\Backend\SSPlus;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class EventNovoPedido
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $separacao;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($separacao)
    {
        $this->separacao = $separacao;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
