<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;


class EventPromocoes implements ShouldBroadcast , ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $promocoes;
    public $channel;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($promocao, $canal)
    {
        $this->promocoes =$promocao ;
        $this->channel = $canal;
    }

    public function broadcastAs()
    {
        return 'promocoes';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('promocoes_'.$this->channel);
    }
}
