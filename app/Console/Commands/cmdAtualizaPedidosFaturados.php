<?php

namespace App\Console\Commands;

use App\Models\Marktplace\MagentoPedidos;
use Illuminate\Console\Command;
use League\OAuth1\Client\Server\Magento;

class cmdAtualizaPedidosFaturados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magento:status_faturado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza status de pedidos que estão faturados no SSPlus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $magento = new MagentoPedidos();
        $magento->atualizaPedidosFaturados();
    }
}
