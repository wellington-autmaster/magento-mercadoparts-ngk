<?php

namespace App\Console\Commands;

use App\Jobs\JobFilaPluggto;
use App\Models\Marktplace\SSProdutosEcommerce;
use Illuminate\Console\Command;
use App\Helpers\Backend\Marktplace\HelperPluggTo;

class cmdMarktplacePluggTo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marktplace:pluggto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comunicação com o Marktplace PluggTo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $FilaProdutos =  JobFilaPluggto::class;
        SSProdutosEcommerce::ProdutosParaAtualizar('pluggto', $FilaProdutos);
    }
}
