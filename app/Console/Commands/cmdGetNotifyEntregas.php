<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\EventMonitorSeparacaoSenha;
use App\Models\Monitor\SsStatusPedido;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDO;
use Psy\Exception\Exception;

class cmdGetNotifyEntregas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pgnotify:entregas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recebe notificações do postgres de entregas de mercadorias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /*PERFORM pg_notify('ntfy_pccmentr',
        '{"empfil":"'||rENT.EMPFIL||'",
								  "clifor":"'||rENT.CODIGO_CLIENTE||'",
								  "descli":"'||rENT.DESCRI_CLIENTE||'",
								  "nronff":"'||rENT.NRONFF||'",
								  "serie":"' ||rENT.SERIE||'",
								  "planil":"'||rENT.PLANIL||'",
								  "entregador-codigo":"'||rENT.codigo_entregador||'",
								  "entregador-nome":"'||rENT.descri_entregador||'",
								  "status":"'||cStatus||'",
								  "codstatus":"'||cCodStatus||'"
		}'); */

        $execs = DB::connection('pgsql');
        while (1) {

            try {
                $execs->select('LISTEN ntfy_pccmentr');
                $result = $execs->getPdo()->pgsqlGetNotify(PDO::FETCH_ASSOC, 1000);
                if ($result) {
                    if (array_key_exists("payload", $result)) {


                        Log::debug('Entrega',
                            (array)$result);

                        $objJson = $result['payload'];
                        $objJson = json_decode($objJson);

                          SsStatusPedido::gravaPedido($objJson); //Grafa informações no banco de dados e retorna a data do pedido

                    //    $modoentregabalcao = Cache::get('modoentregabalcao_' . $objJson->empfil, true);


                      /*  if ($objJson->modoentregabalcao == '3' || $modoentregabalcao == false) {
                            broadcast(new EventMonitorSeparacaoSenha($objJson, $objJson->empfil));
                        };*/


                      //  $totais =  SsStatusPedido::getTotais($objJson->empfil);

                        /*elseif ($modoentregabalcao == false && $objJson->modoentregabalcao == '2'){
                            broadcast(new EventMonitorSeparacaoSenha($objJson,$objJson->empfil));
                        }*/
                    }
                };
                flush();
            } catch (Exception $e) {
                Log::info('Problemas com notificação de entregas');
            }
        }
    }
}
