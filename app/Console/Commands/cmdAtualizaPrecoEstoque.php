<?php

namespace App\Console\Commands;


use App\Http\Controllers\Frontend\MonitorSeparacao\FrtMonitorSeparacaoController;
use App\Jobs\jobAtualizaPrecoEstoque;
use App\Models\Marktplace\SSProdutosEcommerce;
use Illuminate\Console\Command;


class cmdAtualizaPrecoEstoque extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magento:preco';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[Desabilitado]  - Atualiza as prmoções no monitor de separação de mercadorias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $FilaProdutos =  jobAtualizaPrecoEstoque::class;
        SSProdutosEcommerce::ProdutosParaAtualizar('magento23',$FilaProdutos);

    }
}
