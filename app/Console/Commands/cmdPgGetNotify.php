<?php

namespace App\Console\Commands;

use App\Events\Backend\SSPlus\EventNovoPedido;
use App\Events\EventMonitorSeparacaoSenha;
use App\Models\Monitor\SsStatusPedido;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDO;
use Psy\Exception\Exception;


class cmdPgGetNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pgnotify:separacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recebe notificações do postgres de separação de mercadorias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $exemplo = '{"empfil":"0001",
                          "clifor":"0039071",
                          "descli":"1001 MS CARGAS BRASIL LTDA - ME",
                          "nronff":"056067",
                          "serie":"COND",
                          "planil":"00307197",
                          "conferente-separador":"0000035",
                          "modoentregabalcao":"3",
                          "status":"Separacao do pedido concluida",
                          "codstatus":"S0002"
                         }';
        // Recebe configuração

        $classe = [
            0 => 'vermelha',
            1 => 'laranja',
            2 => 'verde',
        ];
        $execs = DB::connection('pgsql');

        $conte = 1;
        while (1) {

            try {
                $execs->select('LISTEN ntfy_pccmces0');
                $result = $execs->getPdo()->pgsqlGetNotify(PDO::FETCH_ASSOC, 1000);
                if ($result) {
                    if (array_key_exists("payload", $result)) {


                        $objJson = $result['payload'];
                        $objJson = json_decode($objJson);

                        event(new EventNovoPedido($objJson));

                        $conferencia = (Cache::get('conferencia_' . $objJson->empfil, false));
                        switch ($objJson->codstatus) {
                            case 'A0001' :
                                $objJson->status = 'Aguardando separação e conferência';
                                $objJson->class = $classe[0];
                                $objJson->data_pedido = Carbon::now()->format("Y-m-d H:i:s");
                                break;
                            case 'D0001' :
                                $objJson->status = 'Pedido cancelado';
                                $objJson->class = $classe[0];
                                break;
                            case 'C0001' :
                                $objJson->status = 'Pedido em conferência';
                                $objJson->class = $classe[1];
                                break;
                            case 'S0001' :
                                $objJson->status = 'Pedido em separação';
                                $objJson->class = $classe[1];
                                break;
                            case 'C0002' :
                                $objJson->status = 'Conferência do pedido concluída';
                                $objJson->class = 'NaoNotificaConferencia';
                                if ($conferencia == true) {  // Se vai até a conferencia a cor é laranja
                                    $objJson->class = $classe[2];
                                }
                                break;
                            case 'S0002' :
                                $objJson->status = 'Separação do pedido concluída';
                                $objJson->class = $classe[2];
                                if ($conferencia == true) {  // Se vai até a conferencia a cor é laranja
                                    $objJson->class = $classe[1];
                                }

                                break;
                        }


                       // $objJson =   SsStatusPedido::gravaPedido($objJson); //Grafa informações no banco de dados e retorna a data do pedido
                           SsStatusPedido::gravaPedido($objJson); //Grafa informações no banco de dados e retorna a data do pedido


                        $modoentregabalcao = Cache::get('modoentregabalcao_' . $objJson->empfil, true);


                        if ($objJson->modoentregabalcao == '3' || $modoentregabalcao == false) {
                            broadcast(new EventMonitorSeparacaoSenha($objJson, $objJson->empfil));
                        };

                        //Dispara evento com total dos pedidos para o monitor de gestores





                        /*elseif ($modoentregabalcao == false && $objJson->modoentregabalcao == '2'){
                            broadcast(new EventMonitorSeparacaoSenha($objJson,$objJson->empfil));
                        }*/
                    }
                };
                flush();
            } catch (Exception $e) {
                Log::info('cai');
            }
        }


    }
}
