<?php

namespace App\Console\Commands;

use App\Models\Marktplace\VeiculoMarca;
use App\Models\Marktplace\VeiculoTipo;
use Illuminate\Console\Command;
use App\Models\Marktplace\Veiculo;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class veiculos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'veiculos:cadastro';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cadastra veiculos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*Cadastra Marcas dos Veiculos*/
        // 1 - Pesquisa pelos tipo de veiculos,
        // depois percorre a API da fipe e puxa todas as marcas de cada tipo de veiculo
        
        $veiculoTipos = VeiculoTipo::all();

        foreach ($veiculoTipos as $tipo) {
            $this->info('Cadastrando ' . $tipo->tipo);
            $client = new GuzzleHttp\Client();
            $res = $client->request('GET', 'http://fipeapi.appspot.com/api/1/' . $tipo->name . '/marcas.json');
            $marcas = json_decode($res->getBody());
            $marcas = (object)$marcas;
            $arrMarcas = [];


            foreach ($marcas as $marca) {
                $arrMarcas[] = [
                    'veiculo_tipo_id' => $tipo->id,
                    'name' => $marca->name,
                    'fipe_name' => $marca->fipe_name,
                    'order' => $marca->order,
                    'key' => $marca->key,
                    'id_fipe' => $marca->id
                ];
            }
            $this->info('FIPE - Gravando as Marcas de '. $tipo->name);
            Log::info('FIPE - Gravando as Marcas de '. $tipo->name);
            DB::table('veiculo_marcas')->insert($arrMarcas);
        }
        /*Fim cadastro de marca dos veiculos*/


        /*Inicio do cadastro de Veiculos*/
        $this->info('Buscando Marcas cadastradas para inicio do cadastro de veiculos');
        $veiculoMarcas = VeiculoMarca::all();

        $this->info($veiculoMarcas->count() . 'Encontrados');
        $i=0;

        foreach ($veiculoMarcas as $marca) {
            $this->info($i . ' de ' . $veiculoMarcas->count() . 'Marcas');

            $client = new GuzzleHttp\Client();
            $res = $client->request('GET', 'http://fipeapi.appspot.com/api/1/carros/veiculos/' . $marca->id_fipe . '.json');
            $veiculos = json_decode($res->getBody());
            $veiculos = (object)$veiculos;
            $arrVeiculos = [];

            foreach ($veiculos as $veiculo) {

                if (!isset($veiculo->fipe_marca)) {
                    continue;
                }

                $arrVeiculos[] = [
                    'veiculo_marca_id' => $marca->id,
                    'fipe_marca' => $veiculo->fipe_marca,
                    'name' => $veiculo->name,
                    'marca' => $veiculo->marca,
                    'key' => $veiculo->key,
                    'id_fipe' => $veiculo->id,
                    'fipe_name' => $veiculo->fipe_name
                ];

            }
            $this->info('FIPE - Gravando os veiculos de '. $marca->name);
            Log::info('FIPE - Gravando os veiculos de '. $marca->name);
            DB::table('veiculos')->insert($arrVeiculos);
        }
        /*Fim do cadastro de veiculos*/


        /*Inicio do cadastro de anos*/
        $this->info('FIPE - Buscando veiculos cadastrados para pesquisar os anos na API');
        Log::info('FIPE - Buscando veiculos cadastrados para pesquisar os anos na API');
        $val = Veiculo::all();

        $ini = $val->count();
        $v = 0;
        $this->info('FIPE - Veiculos cadastrados' .$ini);
        Log::info('FIPE - Veiculos cadastrados' .$ini);
        
        DB::table('veiculos')->orderBy('id')->chunk(100, function ($veiculos) use($ini,$v) {

            $arrAnos = [];
           
            foreach ($veiculos as $veiculo) {
                if (isset($i)) { $i++; } else {$i=0;}

                $this->info('FIPE - Veiculo '.$i.' de '.$ini.' ');
                Log::info('FIPE - Veiculo '.$i.' de '.$ini.' ');

                $veiculo = Veiculo::find($veiculo->id);

                if ($veiculo->GETmarca == null) {
                    return dd($veiculo->GETmarca);
                }
                $client = new GuzzleHttp\Client();
                $res = $client->request('GET', 'http://fipeapi.appspot.com/api/1/carros/veiculo/' . $veiculo->GETmarca->id_fipe . '/' . $veiculo->id_fipe . '.json');
                $detalhes = json_decode($res->getBody());
                $detalhes = (object)$detalhes;


                foreach ($detalhes as $detalhe) {

                    $client = new GuzzleHttp\Client();
                    $res = $client->request('GET', 'http://fipeapi.appspot.com/api/1/carros/veiculo/' . $veiculo->GETmarca->id_fipe . '/' . $veiculo->id_fipe . '/' . $detalhe->id . '.json');
                    $ano = json_decode($res->getBody());
                    $ano = (object)$ano;

                    //  return 'http://fipeapi.appspot.com/api/1/carros/veiculo/' . $veiculo->GETmarca->id_fipe . '/' . $veiculo->id_fipe . '/' . $detalhe->id . '.json';
                    if (!isset($ano->id)) {
                        continue;
                    }

                    $arrAnos[] = [
                        'veiculo_id' => $veiculo->id,
                        'referencia' => $ano->referencia,
                        'fipe_codigo' => $ano->fipe_codigo,
                        'name' => $ano->name,
                        'combustivel' => $ano->combustivel,
                        'marca' => $ano->marca,
                        'ano_modelo' => $ano->ano_modelo,
                        'preco' => $ano->preco,
                        'key' => $ano->key,
                        'id_fipe' => $ano->id
                    ];
                }
            }
            DB::table('veiculo_anos')->insert($arrAnos);

        });
        Log::info('FIPE - Concluido. ');
    }
}
