<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa\Empresa;
use App\Models\Empresa\Configuracao;
use Illuminate\Support\Facades\DB;

class ConfiguracaoSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        Configuracao::truncate();

        $empresas = Empresa::all();

        //Cadastra as empresas no banco local
        foreach ($empresas as $empresa) {

            Configuracao::create([
                'empresa'  => $empresa->codigo,
                'modulo'   => 'Marktplace',
                'configuracao' => 'atualizacao_geral_oc',
                'valor'    => 'false',
                'descricao' =>'Atualização geral irá enviar todos os produtos ativos para o e-commerce
                no proximo horário.<br>
                <b>Obs:</b>  Essa configuração é valida somente para o primeiro envio do proximo horário, 
                 após enviar, a configuração volta ao normal, atualizando apenas produtos modificados no dia. <br>
                <b>Atenção:</b> O Envio de todos os produtos pode sobrecarregar o seu servidor, use somente quando necessário.</small>',
            ]);

            Configuracao::create([
                'empresa'  => $empresa->codigo,
                'modulo'   => 'Marktplace',
                'configuracao' => 'atualizacao_geral_oc_img',
                'valor'    => 'false',
                'descricao' =>'Atualização geral irá enviar todos os produtos ativos para o e-commerce
                no proximo horário.<br>
                <b>Obs:</b>  Essa configuração é valida somente para o primeiro envio do proximo horário, 
                 após enviar, a configuração volta ao normal, atualizando apenas produtos modificados no dia. <br>
                <b>Atenção:</b> O Envio de todos os produtos pode sobrecarregar o seu servidor, use somente quando necessário.</small>',
            ]);

            Configuracao::create([
                'empresa'  => $empresa->codigo,
                'modulo'   => 'Marktplace',
                'configuracao' => 'atualizacao_geral_pt',
                'valor'    => 'false',
                'descricao' =>'Atualização geral irá enviar todos os produtos ativos para o e-commerce
                no proximo horário.<br>
                <b>Obs:</b>  Essa configuração é valida somente para o primeiro envio do proximo horário, 
                após enviar, a configuração volta ao normal, atualizando apenas produtos modificados no dia. <br>
                <b>Atenção:</b> O Envio de todos os produtos pode sobrecarregar o seu servidor, use somente quando necessário.</small>',
            ]);

            Configuracao::create([
                'empresa'  => $empresa->codigo,
                'modulo'   => 'Marktplace',
                'configuracao' => 'atualizacao_geral_pa',
                'valor'    => 'false',
                'descricao' =>'Atualização geral irá enviar todos os produtos ativos para o e-commerce
                no proximo horário.<br>
                <b>Obs:</b>  Essa configuração é valida somente para o primeiro envio do proximo horário, 
                após enviar, a configuração volta ao normal, atualizando apenas produtos modificados no dia. <br>
                <b>Atenção:</b> O Envio de todos os produtos pode sobrecarregar o seu servidor, use somente quando necessário.</small>',
            ]);

            Configuracao::create([
                    'modulo' => 'opencart',
                    'configuracao' => 'enviar_promocao',
                    'empresa' => $empresa->codigo,
                    'valor' => true,
                    'descricao' => 'Ative para enviar promoções do ssplus ao e-commerce'
                ]);
            Configuracao::create([
                    'modulo' => 'opencart',
                    'configuracao' => 'marca_na_descricao',
                    'empresa' => $empresa->codigo,
                    'valor' => true,
                    'descricao' => 'Ative para enviar a marca apos a descrição do produto'
                ]);
            Configuracao::create([
                    'modulo' => 'opencart',
                    'configuracao' => 'atualizar_descricao',
                    'empresa' => $empresa->codigo,
                    'valor' => true,
                    'descricao' => 'Ative para atualizao a descrição (Aplicação) no e-Commerce'
                ]);
            Configuracao::create([
                    'modulo' => 'opencart',
                    'configuracao' => 'inativa_produto_sem_estoque',
                    'empresa' => $empresa->codigo,
                    'valor' => true,
                    'descricao' => 'Ative para inativar produtos sem estoque no e-Commerce'
                ]);

            //---------
            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'status_pedido_importar',
                'empresa' => $empresa->codigo,
                'valor' => 0,
                'descricao' => 'Status do pedido para importar no SSPlus'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'status_pos_importa_pedido',
                'empresa' => $empresa->codigo,
                'valor' => 0,
                'descricao' => 'Status do pedido no e-commerce após importar para o ssplus'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'status_pos_nfe_pedido',
                'empresa' => $empresa->codigo,
                'valor' => true,
                'descricao' => 'Status do pedido no e-commerce após gerar a NF-e no SSPlus'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'limite_credito_cliente',
                'empresa' => $empresa->codigo,
                'valor' => '0.00',
                'descricao' => 'Configure um limite de credito para o cliente'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'atualiza_cliente_ss',
                'empresa' => $empresa->codigo,
                'valor' => true,
                'descricao' => 'Ative para atualizar o cadastro do cliente com base no cadastro do e-commerce'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'forma_pagamento_ss',
                'empresa' => $empresa->codigo,
                'valor' => '00',
                'descricao' => 'Forma de pagamento configurada para e-commerce no SSPlus'
            ]);

            Configuracao::create([
                'modulo' => 'opencart',
                'configuracao' => 'classe_nao_enviar',
                'empresa' => $empresa->codigo,
                'valor' => '',
                'descricao' => 'Classe de produtos para nao enviar ao e-commerce'
            ]);



        }
        $this->enableForeignKeys();
        // Exibe mensagem no terminal
        $this->command->info('Configurações criadas. ');
    }
}
