<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
      //  $this->call(SsStatusPedidoTableSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(AuthTableSeeder::class);
        $this->call(MarktplaceSeeder::class);
        $this->call(ConfiguracaoSeeder::class);
        $this->call(ConfiguracaoMagento23Seed::class);
        $this->call(SeedMonitor::class);

        Model::reguard();
    }
}
