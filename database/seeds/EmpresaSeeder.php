<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Empresa\Empresa;

class EmpresaSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        //Truncar tabela de empresas
        $this->command->info('Limpando registros da tabela de Empresas');
        DB::table('empresas')->truncate();
        
        //empresas auxiliares
        $auxiliares = ['1001','2002','3003','4004','5005','6006','7007','8008','1010',
        '1100','1122','1133'];

        //Seleciona empresas cadastradas no ssplus
        $empresas = DB::connection('pgsql')
            ->table('pcmtemp0')->whereNotIn('codigo',  $auxiliares)->orderBy('codigo', 'asc')->get();

        //Cadastra as empresas no banco local
        foreach ($empresas as $empresa) {

            Empresa::create([
                'codigo' => $empresa->codigo,
                'fantasia' => $empresa->fantas,
                'razao' => $empresa->razao,
                'cgc' => $empresa->cgc,
                'clifort' => $empresa->clifort,
                'ie' => $empresa->cce,
                'endere' => $empresa->endere,
                'bairro' => $empresa->bairro,
                'municipio' => $empresa->munici,
                'estado' => $empresa->estado,
                'cep' => $empresa->cep,
                'fone' => $empresa->fone

            ]);

            // Exibe mensagem no terminal
            $this->command->info('Empresa ' . $empresa->codigo . ' cadastrada.');
        }
        $this->enableForeignKeys();
    }
}
