<?php

use App\Models\Empresa\Empresa;
use App\Modulo;
use Illuminate\Database\Seeder;

class ModulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modulo::truncate();

         $empresas = Empresa::all();

         foreach ($empresas as $empresa) {
             Modulo::create(['descricao'  => 'Opencart', 'empresa' => $empresa->codigo]);
             Modulo::create(['descricao'  => 'Magento', 'empresa' => $empresa->codigo]);
             Modulo::create(['descricao'  => 'Plugg.to', 'empresa' => $empresa->codigo]);
             Modulo::create(['descricao'  => 'Monitor de Senhas', 'empresa' => $empresa->codigo]);
             Modulo::create(['descricao'  => 'Monitor de Pedidos', 'empresa' => $empresa->codigo]);
             Modulo::create(['descricao'  => 'Monitor de Oficina', 'empresa' => $empresa->codigo]);
         }

    }
}
