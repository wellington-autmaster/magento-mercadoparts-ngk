<?php


use App\Models\Empresa\Empresa;
use App\Models\Marktplace\VeiculoTipo;
use Illuminate\Database\Seeder;
use App\Models\Marktplace\Marktplace;

class MarktplaceSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     * Faz a leitura das empresas cadastradas no ssplus e cadastra na plataforma
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        //Cadastra os marktplaces disponiveis
        Marktplace::truncate();

        $empresas = Empresa::all();
        
        foreach($empresas as $empresa)
        {
            Marktplace::create(
                [
                    'codigo' => 'plugto',
                    'nome' => 'Plugg.To',
                    'usuario' => '',
                    'senha' => '',
                    'api_key' => '',
                    'status' => false,
                    'imagem' => '/img/backend/marktplace/logo-plugto.png',
                    'descricao' => 'A Plugg.to integra seus estoques, produtos e vendas entre os maiores marketplaces e seu SSPlus',
                    'url' => 'www.plugg.to',
                    'modification_user_id' => 1,
                    'empresa' => $empresa->codigo,
                ]);
    
                Marktplace::create(
                [
                    'codigo' => 'pecaqui',
                    'nome' => 'Peça Aqui',
                    'usuario' => '',
                    'senha' => '',
                    'api_key' => '',
                    'status' => false,
                    'imagem' => '/img/backend/marktplace/logo-pecaaqui.png',
                    'descricao' => 'Milhares de lojas e vendedores ao seu alcance, oferecendo as peças que você precisa ou deseja para seu carro.',
                    'url' => 'www.pecaaqui.com',
                    'modification_user_id' => 1,
                    'empresa' => $empresa->codigo,
                ]);

            Marktplace::create(
                [
                    'codigo' => 'opencart',
                    'nome' => 'Opencart',
                    'usuario' => '',
                    'senha' => '',
                    'api_key' => '',
                    'status' => false,
                    'imagem' => '/img/backend/marktplace/logo-ecommerce.png',
                    'descricao' => 'Opencart integra seus estoques, produtos e vendas entre os maiores marketplaces e seu SSPlus',
                    'url' => '',
                    'modification_user_id' => 1,
                    'empresa' => $empresa->codigo,
                ]);

            Marktplace::create(
                [
                    'codigo' => 'magento23',
                    'nome' => 'Magento 2.3',
                    'usuario' => '',
                    'senha' => '',
                    'api_key' => '',
                    'status' => false,
                    'imagem' => '/img/backend/marktplace/logo-magento.png',
                    'descricao' => 'Magento integra seus estoques, produtos e imagem com seu SSPlus',
                    'url' => '',
                    'modification_user_id' => 1,
                    'empresa' => $empresa->codigo,
                ]);



        }




        if ($this->command->confirm('Atenção: Deseja recadastrar os veículos?')) {

            $this->disableForeignKeys();
            VeiculoTipo::truncate();

            // Cadastra os tipos de veiculos (carros, motos e caminhoes)
            VeiculoTipo::create(
                  ['tipo' => 'Carros'],
                  ['tipo' => 'Motos'],
                  ['tipo' => 'Caminhoes']
              );
            $this->command->info('Tipos de veiculo (carros, motos e caminhoes)  cadastrado em MarktplaceSeeder.');
            $this->command->info('Preparando-se para pesquisar as Marcas');
            ;
        }




        $this->enableForeignKeys();
    }
}
