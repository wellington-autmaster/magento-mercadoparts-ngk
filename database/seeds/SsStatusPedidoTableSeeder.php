<?php

use App\Models\Monitor\SsStatusPedido;
use App\SsStatusPedido;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SsStatusPedidoTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         factory(SsStatusPedido::class, 100)->create();


        /* SsStatusPedido::create([
             'empresa' => '0001',
             'pedido' => Str::random(6),
             'codigo_cliente' => Str::random(6),
             'codigo_status' => 'A0001',
             'status' => 'Pedido efetuado aguardando separacao e conferencia',
             'data_inicial' => Carbon::now(),
             'data_final' => '',
             'vendedor' => Str::random(6),
             'operador' => Str::random(6),
             'modoentrega' => '3',

         ]);*/

    }
}
