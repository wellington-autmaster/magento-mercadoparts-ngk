<?php

use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(\App\SsStatusPedido::class, function (Faker $faker) {
    return [
        'empresa' => '0001',
        'pedido' => $faker->numberBetween(0,999999),
        'codigo_cliente' => $faker->numberBetween(0,999999),
        'codigo_status' => 'A0001',
        'status' => 'Pedido efetuado aguardando separacao e conferencia',
        'data_inicial' => Carbon::now(),
        'data_final' => Carbon::now(),
        'vendedor' => $faker->numberBetween(0,999999),
        'operador' => $faker->name,
        'modoentrega' => '3',
    ];
});
