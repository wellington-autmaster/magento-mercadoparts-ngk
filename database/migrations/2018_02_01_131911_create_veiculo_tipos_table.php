<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculoTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Cria tabela de tipos de veiculos
        Schema::create('veiculo_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->timestamps();
        });

        // Tabela de Marca de veiculos
        Schema::create('veiculo_marcas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('veiculo_tipo_id')->unsigned();
            $table->string('name');
            $table->string('fipe_name');
            $table->string('order');
            $table->string('key');
            $table->integer('id_fipe');
            $table->timestamps();

            $table->unique('id_fipe');
            $table->foreign('veiculo_tipo_id')->references('id')->on('veiculo_tipos');
        });

        // Tabela de Veiculos
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('veiculo_marca_id')->unsigned();
            $table->string('fipe_marca');
            $table->string('name');
            $table->string('marca');
            $table->string('key');
            $table->integer('id_fipe');
            $table->string('fipe_name');
            $table->timestamps();

            $table->unique('id_fipe');
            $table->foreign('veiculo_marca_id')->references('id')->on('veiculo_marcas');
        });

        // Tabela Ano
        Schema::create('veiculo_anos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('veiculo_id')->unsigned();
            $table->string('referencia');
            $table->string('fipe_codigo');
            $table->string('name');
            $table->string('combustivel');
            $table->string('marca');
            $table->string('ano_modelo');
            $table->string('preco');
            $table->string('key');
            $table->integer('id_fipe');
            $table->timestamps();

         //   $table->unique('id_fipe');
            $table->foreign('veiculo_id')->references('id')->on('veiculos');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculo_anos');
        Schema::dropIfExists('veiculos');
        Schema::dropIfExists('veiculo_marcas');
        Schema::dropIfExists('veiculo_tipos');
    }
}
