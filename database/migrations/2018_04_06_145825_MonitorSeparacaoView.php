<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MonitorSeparacaoView extends Migration
{

    protected $connection = 'pgsql';


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection('pgsql')->statement("
        CREATE  OR REPLACE VIEW monitorseparacao AS SELECT 
        produtividade_conferentes.empfil,
        produtividade_conferentes.planil,
        produtividade_conferentes.data,
        produtividade_conferentes.hora_inicio,
        produtividade_conferentes.hora_fim,
        produtividade_conferentes.codigo,
        Pccmces0.clifor,
        Pccdcli0.descri,
        Pccmces0.nronff,
        Pccmces0.modoentregabalcao
        
        FROM produtividade_conferentes 
        LEFT JOIN Pccmces0  ON produtividade_conferentes.planil = Pccmces0.planil
        LEFT JOIN Pccdcli0  ON 	Pccmces0.clifor =  pccdcli0.codigo
        ORDER BY codigo DESC;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection('pgsql')->statement('DROP VIEW IF EXISTS MonitorSeparacao');
    }
}
