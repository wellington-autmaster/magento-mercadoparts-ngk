<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarktplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marktplaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('nome',100);
            $table->string('usuario',100)->default('');
            $table->string('senha',100)->default('');
            $table->string('api_key')->default('')->nullable();
            $table->string('empresa',4)->default('0001');
            $table->boolean('status')->default(false);
            $table->integer('modification_user_id')->unsigned();
            $table->string('imagem',100);
            $table->string('descricao');
            $table->string('url')->default('')->nullable();
            $table->string('client_id')->default('')->nullable();
            $table->string('client_secret')->default('')->nullable();
            $table->timestamps();

            $table->foreign('modification_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marktplaces');
    }
}
