<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ssproduto_id');
            $table->integer('caracteristica_id')->unsigned();
            $table->string('valor');
            $table->timestamps();

            $table->foreign('caracteristica_id')->references('id')->on('caracteristicas');
            $table->index('ssproduto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_caracteristicas');
    }
}
