<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMonitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('monitors', function (Blueprint $table) {
            $table->boolean('conferencia')->default(false)->comment('Controlar senhas de conferencia');
            $table->integer('qtdsenhasaguardando')->default(10)->comment('Numero de senhas aguardando processamento para exibir no monitor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('conferencia');
            $table->dropColumn('qtdsenhasaguardando');
        });
    }
}
