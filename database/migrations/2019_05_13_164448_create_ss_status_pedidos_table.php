<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSsStatusPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_status_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('pedido');
            $table->string('codigo_cliente');
            $table->string('descricao_cliente');
            $table->dateTime('data_pedido');
            $table->string('modoentrega')->nullable();
            $table->string('status');
            $table->string('status_descricao');
            $table->string('colaborador');
            $table->dateTime('A0001')->nullable()->comment('Pedido efetuado aguardando separação e conferencia');
            $table->dateTime('S0001')->nullable()->comment('Pedido em Separação');
            $table->dateTime('S0002')->nullable()->comment('Separação do pedido concluída ');
            $table->dateTime('C0001')->nullable()->comment('Pedido em conferencia ');
            $table->dateTime('C0002')->nullable()->comment('Conferencia do Pedido concluída ');
            $table->dateTime('E0001')->nullable()->comment('Entrega Liberada (definido o motoboy) ');
            $table->dateTime('E0002')->nullable()->comment('Entrega em Transito (Saiu para Entregar)');
            $table->dateTime('E0003')->nullable()->comment('Entrega Fechada (Motoboy ja voltou) ');
            $table->dateTime('D0001')->nullable()->comment('Pedido cancelado separação e conferencia');
            $table->string('entregador_codigo')->nullable();
            $table->string('entregador_nome')->nullable();
            $table->string('codigo_separador')->nullable();
            $table->string('nome_separador')->nullable();
            $table->string('codigo_conferente')->nullable();
            $table->string('nome_conferente')->nullable();
            $table->boolean('finalizado');
            $table->boolean('cancelado');
            $table->timestamps();

            $table->index(['empresa', 'pedido']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_status_pedidos');
    }
}
