<?php


/**
 * Todas as rotas devem ter prefixo 'admin.'.
 */

Route::group(['namespace' => 'Configuracao'], function () {

    Route::get('configuracao/modulo/{modulo}/configuracao/{configuracao}', 'ConfiguracaoController@GetConfiguracao')->name('getConfiguracao');
    Route::post('configuracao/update', 'ConfiguracaoController@PostConfiguracao')->name('PostConfiguracao');

});