<?php


/**
 * Todas as rotas devem ter prefixo 'admin.'.
 */

Route::group(['namespace' => 'MonitorSeparacao'], function () {

    Route::get('monitor/index','MonitorSeparacaoController@index')->name('monitor.index');
    Route::post('monitor/update','MonitorSeparacaoController@update')->name('monitor.update');




});

