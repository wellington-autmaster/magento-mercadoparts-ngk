<?php


/**
 * Todas as rotas devem ter prefixo 'admin.'.
 */

Route::group(['namespace' => 'Marktplace'], function () {
    Route::get('marktplace', 'MarktplaceController@index')->name('marktplace');
    Route::get('marktplace/editar/{id}', ['uses' => 'MarktplaceController@editar', 'as' =>'marktplace.editar' ]);
    Route::get('marktplace/opencart/callback', ['uses' => 'MarktplaceController@OcCallback', 'as' =>'marktplace.OcCallback' ]);


    Route::get('marktplace/teste', ['uses' => 'MarktplaceController@teste', 'as' =>'marktplace.teste' ]);
    Route::patch('marktplace/update/', ['uses' => 'MarktplaceController@update', 'as' =>'marktplace.update' ]);

    Route::get('pesquisa', 'MarktplaceController@index')->name('marktplace');

    Route::get('marktplace/pecaqui','MarktplaceController@enviaProdutosPecaAqui')->name('enviaProdutosPecaAqui');
    Route::get('marktplace/plugto','MarktplaceController@enviaProdutosPluggTo')->name('enviaProdutosPluggTo');
    Route::get('marktplace/atualiza/produto/{id}','MarktplaceController@atualizaProdutoMarktplace')->name('atualizaProdutoMarktplace');
    Route::get('marktplace/processador','MarktplaceController@processarFila')->name('processarFila');
    Route::get('marktplace/processador/pedidos','MagentoPedidosController@processaFilaPedidos')->name('processarFilaPedidos');
    Route::get('marktplace/opencart/pedidos/status','MarktplaceController@getStatusOc')->name('getStatusOc');


    Route::get('marktplace/aplicacao','MarktplaceController@aplicacaoRemover')->name('aplicacao');


});

Route::get('dashboard', 'DashboardController@index')->name('dashboard');



