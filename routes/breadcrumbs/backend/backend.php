<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(  '<i class=\'fa fa-tachometer\' aria-hidden=\'true\'></i>   Dashboard', route('admin.dashboard'));
});



require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
require __DIR__ . '/marktplace.php';
require __DIR__ . '/monitor-separacao.php';
