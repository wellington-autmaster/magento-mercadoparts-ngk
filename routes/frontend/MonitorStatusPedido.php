<?php


Route::group(['namespace' => 'MonitorStatusPedido'], function () {
    Route::get('monitor/gestores/empresa/id/{empresa_id}', 'FrtMonitorStatusPedidoController@index')->name('monitor.status.pedido');
    Route::get('monitor/gestores/empresa/id/getpedidos/{empresa_id}', 'FrtMonitorStatusPedidoController@getPedidos')->name('monitor.pedidos.getPedidos');
    Route::get('monitor/gestores/empresa/pedidos/painel/{empresa_id}', 'FrtMonitorStatusPedidoController@Pedidos');


});
