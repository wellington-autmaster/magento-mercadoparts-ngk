<?php


Route::group(['namespace' => 'MonitorSeparacao'], function () {
    Route::get('monitor/empresa/id/{empresa_id}', 'FrtMonitorSeparacaoController@monitor')->name('monitor');
    Route::get('monitor/separacao/senhas', 'FrtMonitorSeparacaoController@senhas')->name('monitor.senhas');
    Route::get('monitor/promocoes', 'FrtMonitorSeparacaoController@promocoes')->name('monitor.promocoes');
    Route::get('monitor/verificaSenha', 'FrtMonitorSeparacaoController@verificaSenha')->name('monitor.verificaSenha');
    //Route::get('monitor/teste', 'FrtMonitorSeparacaoController@teste')->name('monitor.teste');
    Route::get('monitor/teste', 'FrtMonitorSeparacaoController@verificaSenha')->name('monitor.teste');
    Route::get('monitor/notify', 'FrtMonitorSeparacaoController@pg_notify_monitor')->name('monitor.pg_notify_monitor');


   //Movido para seu proprio arquivo de rotas Route::get('monitor/gestores/empresa/id/{empresa_id}', 'FrtMonitorPedidosController@index')->name('monitor.pedidos.gestores');

});