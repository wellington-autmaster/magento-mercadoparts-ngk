<?php

return [

    'Oauth1' => [
        'consumer_key'    => env('MAGENTO_CONSUMER_KEY', false),
        'consumer_secret' => env('MAGENTO_CONSUMER_SECRET', false),
        'token'           => env('MAGENTO_TOKEN', false),
        'token_secret'    => env('MAGENTO_TOKEN_SECRET', false),
    ],

    'status_para_importar' => env('MAGENTO_STATUS_PARA_IMPORT_SS', 'processing'),
    'status_apos_importar' => env('MAGENTO_STATUS_APOS_IMPORT_SS', 'em_faturamento'),
    'status_apos_faturar' => env('MAGENTO_STATUS_APOS_FATURAR_SS', 'faturado'),
    'status_apos_cancelar' => env('MAGENTO_STATUS_APOS_CANCELAR_SS', 'processing_cancelado'),


    'url_magento'          => env('MAGENTO_URL', 'jksafiasuhfoiasfhpoajf'),
    'homologacao'          => env('MAGENTO_HOMOLOGACAO', false),
    'seller_id'          => env('MAGENTO_SELLER_ID', '52'),
    'operacao'             => env('MAGENTO_OPERACAO', ''),


];